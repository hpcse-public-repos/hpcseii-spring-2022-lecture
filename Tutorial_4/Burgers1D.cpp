#include <iostream>
#include <cmath>
#include <fstream>
#include <cassert>

struct Simulation
{
    int N;
    double dx;
    double t;
    double * u;
    double * rhs;

    Simulation(int n)
    {
        assert(n>0 && "Grid points must be positive");

        N  = n;
        dx = 1.0 / N;

        for (int i = 0 ; i < N ; i++)
        {
            double x = (i+0.5)*dx;
            u[i] = sin(2*M_PI*x);
        }
    }

    ~Simulation()
    {
    }

    double compute_time_step() const
    {
        double max_u = 0.0;
        for (int i = 0 ; i < N ; i++)
        {
            max_u = std::max(max_u, std::fabs(u[i]) );
        }
        
        assert (max_u > 1e-16);

        const double dt = 0.5*dx/max_u;
        return dt;
    }

    double upwind_derivative(const double u1, const double u2, const double u3) const
    {
        if (u2 > 0) return (u2-u1)/dx;
        else        return (u3-u2)/dx;
    }

    void solve(const double t_max)
    {
        int counter = 0;
        t = 0.0;
        while (t < t_max)
        {
           const double dt = compute_time_step();
           for (int i = 0 ; i < N ; i++)
           {
                int ip1 = i + 1;
                int im1 = i - 1;
                const double dudx = upwind_derivative(u[im1],u[i],u[ip1]);
                rhs[i] = -dt*u[i]*dudx;
           }
           for (int i = 0 ; i < N ; i++)
               u[i] += rhs[i];
           t += dt;
           if (counter % 10 == 0) save_to_file();
           counter ++;
        }
    }

    void save_to_file() const
    {
        std::ofstream myfile;
        myfile.open("example.txt", std::fstream::out | std::fstream::app);
        myfile << t << " " ;
        for (int i = 0; i < N ; i++)
        {
            myfile << u[i] << " ";
        }
        myfile << "\n";
    }
};

int main()
{
  Simulation test(-100);
  test.solve(1.0);
  return 0;
}
