#include <cmath>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <string>

#include "wave.hpp"

//
// Function defining the initial displacement on the grid
//
double f(double x, double y)
{
    double r = (x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5);
    return 1 - std::sin(M_PI * r) * std::exp(-r);
}

WaveEquation::WaveEquation(int a_gridpoints_per_dim,
                           int a_procs_per_dim,
                           double a_t_end,
                           MPI_Comm a_comm)
    : t_end(a_t_end), Ntot(a_gridpoints_per_dim), 
    procs_per_dim(a_procs_per_dim)
{
    h = L / Ntot;
    N = Ntot / procs_per_dim;
    N_halo = N + 2;

    dt = h / 3;
    c_aug = dt * dt / (h * h);

    u.resize(N_halo * N_halo);
    u_old.resize(N_halo * N_halo);
    u_new.resize(N_halo * N_halo);

    //
    // MPI related initializations
    //
    MPI_Comm_size(a_comm, &size);

    int periodic[2] = {true, true};
    // Set no restrictions to the number of processes on any dimension
    nums[0] = 0;
    nums[1] = 0;
    MPI_Dims_create(size, 2, nums);

    MPI_Cart_create(a_comm, 2, nums, periodic, true, &cart_comm);

    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_shift(cart_comm, 0, 1, &rank_minus[0], &rank_plus[0]);
    MPI_Cart_shift(cart_comm, 1, 1, &rank_minus[1], &rank_plus[1]);
    if (rank == 0) 
        std::cout << "(" << nums[0] << ", " << nums[1] << ") processes mapping to a (" << N << ", " << N << ") grid\n";

    //
    // Find its location in the simulation space
    //
    MPI_Cart_coords(cart_comm, rank, 2, &coords[0]);
    origin[0] = N * h * coords[0];
    origin[1] = N * h * coords[1];

    //
    // Set initial conditions (inclusive first time derivative)
    //
    initializeGrid();

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            int idx = (i + 1) * N_halo + (j + 1);
            u_old[idx] = u[idx];
            u_new[idx] = u[idx];
        }
    }
}

void WaveEquation::run()
{
    double t = 0.0;
    unsigned int count = 0;

    //
    // Create datatypes to communicate halo boundaries
    //
    // // Possible solution with following types
    // MPI_Datatype horizontal_halo_t, vertical_halo_t;

    // MPI_Type_contiguous(N, MPI_DOUBLE, &horizontal_halo_t);
    // MPI_Type_commit(&horizontal_halo_t);

    // int blocklength = N;
    // int stride = N_halo;
    // MPI_Type_vector(N, blocklength, stride, MPI_DOUBLE, &vertical_halo_t);
    // MPI_Type_commit(&vertical_halo_t);

    MPI_Datatype SEND_HALO_PLUS[2];
    MPI_Datatype SEND_HALO_MINUS[2];

    MPI_Datatype RECV_HALO_PLUS[2];
    MPI_Datatype RECV_HALO_MINUS[2];

    // Parameters to help in loop
    const int ndims = 2;
    const int order = MPI_ORDER_C;
    const int sizes[2] = {N_halo, N_halo};
    int subsizes[2] = {N, N};
    int starts[2] = {1, 1};

    for (int i = 0; i < ndims; ++i) {

        // Dimension i has now subsize 1, while the rest have subsize N
        subsizes[i] = 1;
        
        // Topmost halo boundary
        starts[i] = 0;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order, MPI_DOUBLE, &RECV_HALO_MINUS[i]);

        // Topmost inner boundary
        starts[i] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order, MPI_DOUBLE, &SEND_HALO_MINUS[i]);

        // Lowermost inner boundary
        starts[i] = N;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order, MPI_DOUBLE, &SEND_HALO_PLUS[i]);

        // // Lowermost halo boundary
        starts[i] = N + 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order, MPI_DOUBLE, &RECV_HALO_PLUS[i]);

        // Reset before next dimesion
        starts[i] = 1;
        subsizes[i] = N;

        // Commit types for dimension i
        MPI_Type_commit(&SEND_HALO_PLUS[i]);
        MPI_Type_commit(&RECV_HALO_PLUS[i]);
        MPI_Type_commit(&SEND_HALO_MINUS[i]);
        MPI_Type_commit(&RECV_HALO_MINUS[i]);

    }

    //
    // Main loop propagating the solution forward in time
    //
    while (t < t_end) {
        if (count % 10 == 9) {
            saveGrid(count);
            double energy_norm = computeSquaredEnergyNorm();
            if (rank == 0)
                std::cout << "t=" << count << " : E(t) = "  << energy_norm << std::endl;
        }

        //
        // Send and receive halo boundaries
        //
        MPI_Request request[4 * ndims];
        for (int d = 0; d < ndims; ++d) {
            MPI_Irecv(u.data(), 1, RECV_HALO_MINUS[d], rank_minus[d], d, cart_comm, &request[2 * d]);
            MPI_Irecv(u.data(), 1, RECV_HALO_PLUS[d], rank_plus[d], d + 1, cart_comm, &request[2 * d + 1]);
        }

        for (int d = 0; d < ndims; ++d) {
            MPI_Isend(u.data(), 1, SEND_HALO_PLUS[d], rank_plus[d], d, cart_comm, &request[2 * (d + ndims)]);
            MPI_Isend(u.data(), 1, SEND_HALO_MINUS[d], rank_minus[d], d + 1, cart_comm, &request[2 * (d + ndims) + 1]);
        }

        MPI_Waitall(4 * ndims, &request[0], MPI_STATUSES_IGNORE);

        //
        // Update the cells with FD stencil
        //
        for (int i = 1; i < N + 1; ++i) {
            for (int j = 1; j < N + 1; ++j) {
                applyStencil(i, j);
            }
        }

        //
        // Swap vectors
        //
        u_old.swap(u);
        u.swap(u_new);

        //
        // Update time
        //
        t += dt;
        count++;
    }

    //
    // Free communication datatypes
    //
    for (int d = 0; d < 2; d++) {
        MPI_Type_free(&RECV_HALO_PLUS[d]);
        MPI_Type_free(&RECV_HALO_MINUS[d]);
        MPI_Type_free(&SEND_HALO_PLUS[d]);
        MPI_Type_free(&SEND_HALO_MINUS[d]);
    }
}

void WaveEquation::initializeGrid()
{
    double x_pos, y_pos;
    for (int i = 0; i < N; ++i) {
        x_pos = origin[0] + i * h + 0.5 * h;
        for (int j = 0; j < N; ++j) {
            y_pos = origin[1] + j * h + 0.5 * h;
            u[(i + 1) * N_halo + (j + 1)] = f(x_pos, y_pos);
        }
    }
}

double WaveEquation::computeSquaredEnergyNorm() const
{
    double energy_norm_m = 0.0;
    double energy_norm_a = 0.0;
    
    for (int i = 1; i < N + 1; ++i)
        for (int j = 1; j < N + 1; ++j) {
              energy_norm_m += (u[i * N_halo + j] - u_old[i * N_halo + j] ) * (u[i * N_halo + j] - u_old[i * N_halo + j] );
              energy_norm_a += (u[(i + 1) * N_halo + j] - u[(i - 1) * N_halo + j]) * (u[(i + 1) * N_halo + j] - u[(i - 1) * N_halo + j])
                            +  (u[i * N_halo + (j + 1)] - u[i * N_halo + (j - 1)]) * (u[i * N_halo + (j + 1)] - u[i * N_halo + (j - 1)]);
        }

    double energy_norm = 0.5 *(energy_norm_m / c_aug + 0.25 * energy_norm_a);

    MPI_Reduce((rank == 0) ? MPI_IN_PLACE : &energy_norm, &energy_norm, 1, MPI_DOUBLE, MPI_SUM, 0, cart_comm);

    return energy_norm;
}

void WaveEquation::applyStencil(int i, int j)
{
    int center = i * N_halo + j;
    u_new[center] =
        2.0 * u[center] - u_old[center] +
        c_aug * (u[(i + 1) * N_halo + j] + u[(i - 1) * N_halo + j] +
                 u[i * N_halo + (j + 1)] + u[i * N_halo + (j - 1)] -
                 4.0 * u[center]);
}

void WaveEquation::saveGrid(int timestep) const
{
    std::stringstream ss;
    ss << "./output/wave_" << std::setfill('0') << std::setw(3) << timestep
       << ".bin";
    std::string fname = ss.str();

    // Create derived datatype for interior grid (output grid)
    MPI_Datatype grid;
    const int start[2] = {1, 1};
    const int arrsize[2] = {N_halo, N_halo};
    const int gridsize[2] = {N, N};

    MPI_Type_create_subarray(
        2, arrsize, gridsize, start, MPI_ORDER_C, MPI_DOUBLE, &grid);
    MPI_Type_commit(&grid);

    // Create derived type for file view
    MPI_Datatype view;
    const int startV[2] = {coords[0] * N, coords[1] * N};
    const int arrsizeV[2] = {nums[0] * N, nums[1] * N};
    const int gridsizeV[2] = {N, N};

    MPI_Type_create_subarray(
        2, arrsizeV, gridsizeV, startV, MPI_ORDER_C, MPI_DOUBLE, &view);
    MPI_Type_commit(&view);

    MPI_File fh;

    MPI_File_open(cart_comm,
                  fname.c_str(),
                  MPI_MODE_CREATE | MPI_MODE_WRONLY,
                  MPI_INFO_NULL,
                  &fh);

    MPI_File_set_view(fh, 0, MPI_DOUBLE, view, "native", MPI_INFO_NULL);
    MPI_File_write_all(fh, u.data(), 1, grid, MPI_STATUS_IGNORE);
    MPI_File_close(&fh);
    MPI_Type_free(&grid);
}

WaveEquation::~WaveEquation() { 
  MPI_Comm_free(&cart_comm); 
}
