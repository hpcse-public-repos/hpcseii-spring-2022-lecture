#include <iostream>
#include <mpi.h>

/**
 * Taken from https://www.rookiehpc.com/mpi/docs/mpi_type_create_resized.php
 * @brief Illustrates how to resize an MPI datatype.
 * @details This program is meant to be run with 3 processes. The MPI process 0
 * holds a 2D array of 12 integers; 4 rows times 3 columns, and scatters columns
 * across all three MPI processes as illustrated below:
 *
 *  Elements of a row
 * <----------------->
 * +-----+-----+-----+ ^
 * |  0  |  1  |  2  | |
 * +-----+-----+-----+ |
 * |  3  |  4  |  5  | | Elements of a column
 * +-----+-----+-----+ |
 * |  6  |  7  |  8  | |
 * +-----+-----+-----+ |
 * |  9  |  10 |  11 | |
 * +-----+-----+-----+ v
 *
 * MPI process 0     MPI process 1     MPI process 2
 *    +-----+           +-----+           +-----+
 *    |  0  |           |  1  |           |  2  |
 *    +-----+           +-----+           +-----+
 *    |  3  |           |  4  |           |  5  |
 *    +-----+           +-----+           +-----+
 *    |  6  |           |  7  |           |  8  |
 *    +-----+           +-----+           +-----+
 *    |  9  |           |  10 |           |  11 |
 *    +-----+           +-----+           +-----+
 *
 * The global array held on MPI process 0 has been declared such that elements
 * of the same row are contiguous in memory, in other words, here is the memory
 * layout of this global array:
 *
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 * |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  10 |  11 |
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 *
 * However the objective here is to scatter columns across the 3 MPI processes.
 * If we identify columns with letters, here is how they are scattered in
 * memory:
 *
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 * |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 *
 * To extract a column, we need the following vector:
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 * |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 * <----->           <----->           <----->           <----->
 * 1st block        2nd block         3rd block        4th block
 *
 * ^                  <--------------->                        ^
 * | Start of          Stride: distance                 End of |
 * | 1st block       between the start of           last block |
 * |                two consecutive blocks                     |
 * |                                                           |
 * <----------------------------------------------------------->
 *          Extent, as calculated by MPI_Type_vector
 *
 *
 * When a vector is repeated, as part of a MPI collective for instance, the
 * vector extent is used to determine the start position of the next vector.
 * This vector extent is calculated by MPI_Type_vector as the distance between
 * the start of the 1st block and the end of the last block. This means that the
 * second vector begins only after the last block of the first vector. Although
 * this makes sense when vectors are made of contiguous data, in our case it
 * does not generate the repeating pattern we want:
 *
 * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
 * |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |
* +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
* ^                                                           ^
* |                                                           |
* +-----------------------------------------------------------+
*                 First vector effectively                    ^
*             extracting all cells of column A                |
*                                                             +---------...
*                                                              Second vector,
    *                                                         supposed to extract
    *                                                       all cells of column B
    *
    * Not only did the second vector missed some of the cells of column B, but
    * having started much further away in the buffer than it was meant to, it is
    * likely to result in a segmentation fault, similarly for C. In our case, we
    * want an vectors to have an interleaved pattern and begin after the first
    * block of the previous vector, as follows:
    *
    * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
    * |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |  A  |  B  |  C  |
    * +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
    * ^     ^     ^
    * |     |     |
    * |     |     | Start of 3rd vector
    * |     |
    * |     | Start of 2nd vector
    * |
    * | Start of 1st vector
    *
    * We therefore need to modify the vector extent accordingly and set it to the
    * size of 1 block, this is where we need the MPI_Type_create_resized routine.
    *
    **/
int main(int argc, char* argv[]) {

    /**********************************************************
    *  Initial Setup
    **********************************************************/
    MPI_Init(&argc, &argv);

    // Get local rank
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Get the number of processes and check only 3 processes are used
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if(size != 3) {
        std::cerr << "This application is meant to be run with 3 processes." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    // Number of cells per column and row.
    const int CELLS_PER_COLUMN = 4, CELLS_PER_ROW = 3;


    /**************************************************************
    *   Start - Coding Part IV
    *   TODO  - Create the vector datatype
    **************************************************************/
    MPI_Datatype tmp;

    /**************************************************************
    *   End - Coding Part IV
    **************************************************************/


    /**************************************************************
    *   Start - Coding Part IV
    *   TODO  - Resize it to make sure it is interleaved when repeated
    **************************************************************/
    MPI_Datatype column_t;

    /**************************************************************
    *   End - Coding Part IV
    **************************************************************/


    /**********************************************************
    *  Run distribution
    **********************************************************/

    // All ranks receive their data in the column
    int column[CELLS_PER_COLUMN];

    // The root initializes and distribute the columns as a matrix
    if(rank == 0) {

        // Declare and initialise the full matrix
        int full_matrix[CELLS_PER_COLUMN*CELLS_PER_ROW];
        for(int i = 0; i < CELLS_PER_COLUMN; i++)
            for(int j = 0; j < CELLS_PER_ROW; j++)
                full_matrix[i*CELLS_PER_ROW + j] = i * CELLS_PER_ROW + j;

        // Scatter the columns
        MPI_Scatter(full_matrix, 1, column_t, column, CELLS_PER_COLUMN, MPI_INT, 0, MPI_COMM_WORLD);
    }

    // Non-root ranks just receive columns ditributed by scattering
    else
        MPI_Scatter(nullptr, 1, column_t, column, CELLS_PER_COLUMN, MPI_INT, 0, MPI_COMM_WORLD);

    // Each rank print their columns
    std::cout << "MPI process" <<  rank << "received column made of cells "
              << column[0] << ", " << column[1] << ", " << column[2] << ", " << column[3] << std::endl;


    /**********************************************************
    *  Finilizing and clean up
    **********************************************************/
    MPI_Type_free(&column_t);
    MPI_Finalize();

    return 0;
}
