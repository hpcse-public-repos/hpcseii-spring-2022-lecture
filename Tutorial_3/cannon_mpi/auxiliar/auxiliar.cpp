#include "auxiliar.hpp"
#include <mpi.h>
#include <iostream>
#include <cmath>

void initializeMatrices(double* A, double* B, const int n, const int N, const int rankx, const int ranky) {
    
    // Initialize value and align for A, B
    double *aptr, *bptr;
    aptr = A; bptr = B;
    for(int i = n*rankx; i < n*(rankx+1); i++)
        for(int j = n*ranky; j < n*(ranky+1); j++) {
            *aptr = 1.0/( (i + ranky * n) % N + j + 1);
            *bptr = 1.0/(i + (j + rankx * n ) % N + 1);
            aptr++; bptr++;
        }

}

bool verifyMatMulResults(double* cptr, const int n, const int N, const int rankx, const int ranky, double execTime) {
    
    // Main rank print verification message
    if (rankx == 0 && ranky == 0) 
        std::cout << "Verifying Result... ";

    // Test if any element is off by more than tolerance
    double tolerance = 1e-6;
    bool error = false;

    // Loop over matrix and test difference between true and computed value
    for(int i = n*rankx; i < n*(rankx+1) && !error; i++)
        for(int j = n*ranky; j < n*(ranky+1) && !error; j++, cptr++) {
            double tmp = 0;
            for(int k = 0; k < N; k++) 
                tmp += 1.0 / ((i + k + 1) * (k + j + 1));
            error = fabs(*cptr-tmp) > tolerance;
        }

    // If there was a too large deviation locally, then tempErr gets the value 1
    // Otherwise, it gets the value 0
    // Collect the max locally at rank 0
    int tempErr = static_cast<int>(error);
    MPI_Reduce(rankx == 0 && ranky == 0 ? MPI_IN_PLACE : &tempErr, &tempErr, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

    if (rankx == 0 && ranky == 0) {

        // If test is not passed print an error message
        if (tempErr == 1)
            std::cerr << "[Error] Verification Failed!" << std::endl;

        // Print that test was passed and also some statistics
        double gflops = (((2e-9)*N)*N)*N/execTime;
        std::cout << "Passed!" << std::endl;
        std::cout << "Execution time: " << execTime << "s"  << std::endl;
        std::cout << "GFlop/s: " << gflops << std::endl;

    }

    return tempErr == 0;

}
