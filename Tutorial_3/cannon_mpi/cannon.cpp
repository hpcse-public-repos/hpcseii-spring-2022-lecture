#include <mpi.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <utility>
#include "auxiliar/auxiliar.hpp"

extern "C" void dgemm_(const char *transa, const char *transb, const int *m, const int *n, const int *k, const double *alpha, const double *a, const int *lda, const double *b, const int *ldb, const double *beta, double *c, const int *ldc);

int main(int argc, char* argv[]) {

    /**********************************************************
    *  Initial Setup
    **********************************************************/
    const size_t N = 1024;

    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);


    /**********************************************************
    *  Verifying that number of processes is a square number
    **********************************************************/

    int p = sqrt(size);
    if (size != p*p) {
        std::cerr << "[Error] Number of processors must be a square integer." << std::endl; 
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    
    /**************************************************************
    *   Start - Coding Part I
    *   TODO  - Create cartesian communicator and compute neighbours
    *           Compute local position in communicator (rankx, ranky)
    **************************************************************/
    int rankx, ranky;

    /**************************************************************
    *   End - Coding Part I
    **************************************************************/


    /********************************************
    *   Initializing Matrices
    *******************************************/

    // Determining local matrix side size (n)
    const int n = N / p;

    // Allocating space for local matrices A, B, C
    double *A,*B,*C;
    A    = (double*) calloc (n*n, sizeof(double));
    B    = (double*) calloc (n*n, sizeof(double));
    C    = (double*) calloc (n*n, sizeof(double));

    // Allocating space for recieve buffers for A and B local matrices
    double *tmpA, *tmpB;
    tmpA = (double*) calloc (n*n, sizeof(double));
    tmpB = (double*) calloc (n*n, sizeof(double));

    // Initializing values for input matrices A and B
    initializeMatrices(A, B, n, N, rankx, ranky);

  /**************************************************************
    *   Running Cannon's Matrix-Matrix  Multiplication Algorithm
    **************************************************************/

    if (rank == 0) 
        std::cout << "Running Matrix-Matrix Multiplication..." << std::endl;

    
    /**************************************************************
    *   Start - Coding Part II
    *   TODO  - Create subarray type
    **************************************************************/

    /**************************************************************
    *   End - Coding Part II
    **************************************************************/


    // Registering initial time. It's important to precede this with a barrier to make sure
    // all ranks are ready to start when we take the time.
    MPI_Barrier(MPI_COMM_WORLD);
    double execTime = -MPI_Wtime();

    // Compute first sub-matrix product
    // one is just a placeholder in order to pass the value to BLAS
    const double one = 1.0;
    dgemm_("N", "N", &n, &n, &n, &one, A, &n, B, &n, &one, C, &n);

    for(int step = 1; step < p; step++) {

    
        /**************************************************************
        *   Start - Coding Part II
        *   TODO  - Send submatrices A and B with subarray data type
        **************************************************************/

        /**************************************************************
        *   End - Coding Part II
        **************************************************************/

        // Compute sub-matrix multiplication
        dgemm_("N", "N", &n, &n, &n, &one, A, &n, B, &n, &one, C, &n);

    }

    // Registering final time. It's important to precede this with a 
    // barrier to make sure all ranks have finished before we take the time.
    MPI_Barrier(MPI_COMM_WORLD);
    execTime += MPI_Wtime();


    /**************************************************************
    *   Verification Stage
    **************************************************************/

    if(!verifyMatMulResults(C, n, N, rankx, ranky, execTime))
        MPI_Abort(MPI_COMM_WORLD, -1);
    
    
    free(A); free(B); free(C); free(tmpA); free(tmpB);
    

    /**************************************************************
    *   Start - Coding Part II
    *   TODO  - Free created MPI structures
    **************************************************************/

    /**************************************************************
    *   End - Coding Part II
    **************************************************************/

    return MPI_Finalize();

}
