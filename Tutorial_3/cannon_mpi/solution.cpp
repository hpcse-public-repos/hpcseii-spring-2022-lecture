#include <mpi.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <utility>
#include "auxiliar/auxiliar.hpp"

extern "C" void dgemm_(const char *transa, const char *transb, const int *m, const int *n, const int *k, const double *alpha, const double *a, const int *lda, const double *b, const int *ldb, const double *beta, double *c, const int *ldc);

int main(int argc, char* argv[]) {

    /**********************************************************
    *  Initial Setup
    **********************************************************/
    const size_t N = 1024;

    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);


    /**********************************************************
    *  Verifying that number of processes is a square number
    **********************************************************/

    int p = sqrt(size);
    if (size != p*p) {
        std::cerr << "[Error] Number of processors must be a square integer." << std::endl; 
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    
    /**************************************************************
    *   Start - Coding Part I
    *   TODO  - Create cartesian communicator and compute neighbours
    *           Compute local position in communicator (rankx, ranky)
    **************************************************************/

    const int ndims = 2;
    int dims[] = {0, 0};    // have to set zero, so as not to set a fixed dimension
    int periodic[] = {true, true};
    MPI_Dims_create (size, ndims, dims);

    MPI_Comm cart_comm;
    MPI_Cart_create (MPI_COMM_WORLD, ndims, dims, periodic, true, &cart_comm);

    MPI_Comm_rank(cart_comm, &rank);

    int coords[ndims];
    MPI_Cart_coords(cart_comm, rank, ndims, coords);
    const int rankx = coords[0];
    const int ranky = coords[1];

    // Determine neighboring ranks
    int up, down, left, right;
    MPI_Cart_shift (cart_comm, 0, 1, &left, &right);
    MPI_Cart_shift (cart_comm, 1, 1, &down, &up);

    /**************************************************************
    *   End - Coding Part I
    **************************************************************/


    /********************************************
    *   Initializing Matrices
    *******************************************/

    // Determining local matrix side size (n)
    const int n = N / p;

    // Allocating space for local matrices A, B, C
    double *A,*B,*C;
    A    = (double*) calloc (n*n, sizeof(double));
    B    = (double*) calloc (n*n, sizeof(double));
    C    = (double*) calloc (n*n, sizeof(double));

    // Allocating space for recieve buffers for A and B local matrices
    double *tmpA, *tmpB;
    tmpA = (double*) calloc (n*n, sizeof(double));
    tmpB = (double*) calloc (n*n, sizeof(double));

    // Initializing values for input matrices A and B
    initializeMatrices(A, B, n, N, rankx, ranky);

  /**************************************************************
    *   Running Cannon's Matrix-Matrix  Multiplication Algorithm
    **************************************************************/

    if (rank == 0) 
        std::cout << "Running Matrix-Matrix Multiplication..." << std::endl;

    
    /**************************************************************
    *   Start - Coding Part II
    *   TODO  - Create subarray type
    **************************************************************/

    // Create new data type
    MPI_Datatype matrix_t;

    // There are many was of solving this problem

    // Using MPI_Type_contiguous
    // MPI_Type_contiguous (n * n, MPI_DOUBLE, &matrix_t);
    
    // Using MPI_Type_vector - Specifying both blocklenght and stride explicitly
    // int blocklen = 1;
    // int stride = 1;
    // MPI_Type_vector(n*n, blocklen, stride, MPI_DOUBLE, &matrix_t);

    // Using MPI_Type_create_subarray - The most generic method of them all
    int sizes[] = {n, n};
    int subsizes[] = {n, n};
    int starts[] = {0, 0};
    int order = MPI_ORDER_C;
    int blocklen = 1;
    int stride = 1;
    MPI_Type_create_subarray (ndims, sizes, subsizes, starts, order, MPI_DOUBLE, &matrix_t);

    // Commit is independent of how the type was created
    MPI_Type_commit (&matrix_t);

    /**************************************************************
    *   End - Coding Part II
    **************************************************************/


    // Registering initial time. It's important to precede this with a barrier to make sure
    // all ranks are ready to start when we take the time.
    MPI_Barrier(MPI_COMM_WORLD);
    double execTime = -MPI_Wtime();

    // Compute first sub-matrix product
    // one is just a placeholder in order to pass the value to BLAS
    const double one = 1.0;
    dgemm_("N", "N", &n, &n, &n, &one, A, &n, B, &n, &one, C, &n);

    for(int step = 1; step < p; step++) {

    
        /**************************************************************
        *   Start - Coding Part II
        *   TODO  - Send submatrices A and B with subarray data type
        **************************************************************/

        // First issuing requests and then sending allows us to 
        // receive data while waiting on sending 
        MPI_Request request[2];
        MPI_Irecv(tmpA, 1, matrix_t, right, 0, cart_comm, &request[0]);
        MPI_Irecv(tmpB, 1, matrix_t, down, 1, cart_comm, &request[1]);

        MPI_Send(A, 1, matrix_t, left, 0, cart_comm);
        MPI_Send(B, 1, matrix_t, up, 1, cart_comm);

        MPI_Waitall(2, request, MPI_STATUS_IGNORE);

        std::swap(A, tmpA);
        std::swap(B, tmpB);

        /**************************************************************
        *   End - Coding Part II
        **************************************************************/

        // Compute sub-matrix multiplication
        dgemm_("N", "N", &n, &n, &n, &one, A, &n, B, &n, &one, C, &n);

    }

    // Registering final time. It's important to precede this with a 
    // barrier to make sure all ranks have finished before we take the time.
    MPI_Barrier(MPI_COMM_WORLD);
    execTime += MPI_Wtime();


    /**************************************************************
    *   Verification Stage
    **************************************************************/

    if(!verifyMatMulResults(C, n, N, rankx, ranky, execTime))
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    
    
    free(A); free(B); free(C); free(tmpA); free(tmpB);
    

    /**************************************************************
    *   Start - Coding Part II
    *   TODO  - Free created MPI structures
    **************************************************************/
    MPI_Comm_free(&cart_comm);
    MPI_Type_free(&matrix_t);

    /**************************************************************
    *   End - Coding Part II
    **************************************************************/

    return MPI_Finalize();

}
