#include <iostream>
#include <mpi.h>
#include <list>
#include <vector>
#include <chrono>
#include <thread>

// StangeStruct is the base class
struct StangeStruct {
    bool flag;
    double values[2];
    int count;
    double redundant;
};

// Helper function to initialize a list
std::list<StangeStruct> get_list(const int N, const int a) {
    
    // List to initialize
    std::list<StangeStruct> list;

    // Push N complicated strange elements into the list
    for(int i = 1; i <= N; ++i)
        list.push_back({
            i % 2 == a,
            {i + a, i + a},
            i + a,
            i + a
    });

    return list;

}

// Test utlity function - verifies that exactly the fields flag and count are sent
// Test only works if we have exactly two ranks
bool check(const std::list<StangeStruct>& listA, const std::list<StangeStruct>& listB) {
    
    auto currA = listA.begin();
    auto currB = listB.begin();
    for(; currA != listA.end(); ++currA, ++currB)
        if(currA->flag != currB->flag
        || currA->values[0] == currB->values[0]
        || currA->values[1] == currB->values[1]
        || currA->count != currB->count
        || currA->redundant == currB->redundant
    )
        return false;

    return true;

}


int main(int argc, char **argv) {
    
    /**********************************************************
    *  Initial Setup
    **********************************************************/
    MPI_Init(&argc, &argv);

    // Get local rank
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Get the number of processes and check only 3 processes are used
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if(size != 2) {
        std::cerr << "This application is meant to be run with 2 processes." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    
    /**************************************************************
    *   Start - Coding Part III
    *   TODO  - Create the base type for sending StangeStruct
    *           You should only send flag and count;
    *           values and redundant should be ignored
    **************************************************************/
    MPI_Datatype base_type;

    /**************************************************************
    *   End - Coding Part III
    **************************************************************/
    

    /**********************************************************
    *  Setup list of StangeStruct
    **********************************************************/
    const int N = 10;
    std::list<StangeStruct> list = get_list(N, rank);


    /**************************************************************
    *   Start - Coding Part III
    *   TODO  - Initialize datatype for sending list
    **************************************************************/
    MPI_Datatype list_type;

    /**************************************************************
    *   End - Coding Part III
    **************************************************************/


    /**********************************************************
    *  Send full from rank 1 and receive at rank 0
    **********************************************************/

    // Notice that the start of the buffer in both MPI calls is MPI_BOTTOM
    if(rank == 0) {

        MPI_Recv(MPI_BOTTOM, 1, list_type, 1, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        // Test if sent only the specified fields
        if(check(list, get_list(N, 1)))
            std::cout << "Success - Test passed" << std::endl;
        else {
            std::cerr << "Error - Test not passed" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }

    }

    else
        MPI_Send(MPI_BOTTOM, 1, list_type, 0, 42, MPI_COMM_WORLD);
    
    
    /**********************************************************
    *  Finalizing and clean up
    **********************************************************/
    MPI_Type_free(&base_type);
    MPI_Type_free(&list_type);
    MPI_Finalize();

    return 0;
}
