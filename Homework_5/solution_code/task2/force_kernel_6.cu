#include <cuda_runtime.h>

__global__ void computeForcesKernel(int N, const double3 *p, double3 *f) {
    extern __shared__ double3 pCache[];

    const int full_blk = (N / blockDim.x) * blockDim.x;
    const int remainder = N - full_blk;

    for(int idx = blockIdx.x * blockDim.x + threadIdx.x;
            idx < N;
            idx += gridDim.x * blockDim.x){

        int stride;
        if (idx < full_blk) {
            stride = blockDim.x;
        } else {
            // applies only for the very last thread block.  Since remainder <=
            // blockDim.x the inner loop below is more work for this block and
            // therefore imbalanced.  Might induce a performance penalty here
            // and grid-strided may not be the best solution.
            stride = remainder;
        }

        // define local variables
        double3 ftot{0.0, 0.0, 0.0};
        double3 myP = p[idx];

        // loop through all particles in N/stride steps
        for (int offset = 0; offset < N; offset += stride) {
            // Check that we don't go over range of N
            int interval_len = min(stride, N - offset);

            // copy the values into shmem
            if(threadIdx.x < interval_len){
                pCache[threadIdx.x] = p[offset + threadIdx.x];
            }

            __syncthreads();

#pragma unroll (16)
            for (int i = 0; i < interval_len; ++i) {
                double dx = pCache[i].x - myP.x;
                double dy = pCache[i].y - myP.y;
                double dz = pCache[i].z - myP.z;
//                double dx = __dsub_rn(pCache[i].x, myP.x);
//                double dy = __dsub_rn(pCache[i].y, myP.y);
//                double dz = __dsub_rn(pCache[i].z, myP.z);
                double inv_r = rsqrt(1e-150 + dx * dx + dy * dy + dz * dz);
                double inv_rrr = inv_r * inv_r * inv_r;
                ftot.x += dx * inv_rrr;
                ftot.y += dy * inv_rrr;
                ftot.z += dz * inv_rrr;
//                ftot.x += __dmul_rn(dx, inv_rrr);
//                ftot.y += __dmul_rn(dy, inv_rrr);
//                ftot.z += __dmul_rn(dz, inv_rrr);
            }

            __syncthreads();

        }
        f[idx] = ftot;
    }
}

void computeForces(int N, const double3 *p, double3 *f) {
    constexpr int numThreads = 256;
    int numBlocks = (N + numThreads - 1) / numThreads;
    int shMemSize = numThreads * sizeof(double3);
    computeForcesKernel<<<numBlocks, numThreads, shMemSize>>>(N, p, f);
}
