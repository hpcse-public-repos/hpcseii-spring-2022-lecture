#include <cuda_runtime.h>

__device__ void intervalInteraction(double3 *pCache, double3 *myP, double3 *ftot, int i){
    double dx = pCache->x - myP->x;
    double dy = pCache->y - myP->y;
    double dz = pCache->z - myP->z;
    double inv_r = rsqrt(1e-150 + dx * dx + dy * dy + dz * dz);
    double inv_rrr = inv_r * inv_r * inv_r;
    ftot->x += dx * inv_rrr;
    ftot->y += dy * inv_rrr;
    ftot->z += dz * inv_rrr;
}

__global__ void computeForcesKernel(int N, const double3 *p, double3 *f) {
    extern __shared__ double3 pCache[];

    const int full_blk = (N / blockDim.x) * blockDim.x;
    const int remainder = N - full_blk;

    for(int idx = blockIdx.x * blockDim.x + threadIdx.x;
            idx < N;
            idx += gridDim.x * blockDim.x){

        // define local variables
        double3 ftot{0.0, 0.0, 0.0};
        double3 myP = p[idx];
        int stride;

        if (idx < full_blk) {
            stride = blockDim.x;
        } else {
            // applies only for the very last thread block.  Since remainder <=
            // blockDim.x the inner loop below is more work for this block and
            // therefore imbalanced.  Might induce a performance penalty here
            // and grid-strided may not be the best solution.
            stride = remainder;
        }

        // loop through all particles in N/stride steps
        for (int offset = 0; offset < N; offset += stride) {
            // check that we don't go over range of N
            int interval_len = min(stride, N - offset);

            // Copy the values into shmem
            if(threadIdx.x < interval_len){
                pCache[threadIdx.x] = p[offset + threadIdx.x];
            }

            __syncthreads();
            for (int i = 0; i < interval_len; i+=8) {
                intervalInteraction(&pCache[i+0], &myP, &ftot, i+0);
                intervalInteraction(&pCache[i+1], &myP, &ftot, i+1);
                intervalInteraction(&pCache[i+2], &myP, &ftot, i+2);
                intervalInteraction(&pCache[i+3], &myP, &ftot, i+3);

                intervalInteraction(&pCache[i+4], &myP, &ftot, i+4);
                intervalInteraction(&pCache[i+5], &myP, &ftot, i+5);
                intervalInteraction(&pCache[i+6], &myP, &ftot, i+6);
                intervalInteraction(&pCache[i+7], &myP, &ftot, i+7);
            }

            // finish last iterations
            int mod = interval_len&~7;
            for(int i = mod; i < interval_len; ++i){
                intervalInteraction(&pCache[i], &myP, &ftot, i);
            }


            __syncthreads();

        }
        f[idx].x = ftot.x;
        f[idx].y = ftot.y;
        f[idx].z = ftot.z;
    }
}

void computeForces(int N, const double3 *p, double3 *f) {
    constexpr int numThreads = 1024;
    int numBlocks = (N + numThreads - 1) / numThreads;
    int shMemSize = numThreads * sizeof(double3);
    computeForcesKernel<<<numBlocks, numThreads, shMemSize>>>(N, p, f);
}
