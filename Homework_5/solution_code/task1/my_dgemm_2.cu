#include <cuda_runtime.h>
#include "utils.h"

#define BLOCK_SIZE 16

__global__ void sharedDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
    // Block row and column
    const int blockRow = blockIdx.y * blockDim.y;
    const int blockCol = blockIdx.x * blockDim.x;

    // Each thread computes one element of Csub
    // by accumulating results into Cvalue
    double Cvalue = 0.;

    // Thread row and column within Csub
    const int threadRow = threadIdx.y;
    const int threadCol = threadIdx.x;

    // Global row and column of C computed by this thread
    const int globRow = blockRow + threadRow;
    const int globCol = blockCol + threadCol;

    // Static declaration of shared memory to store Asub and Bsub respectively
    __shared__ double As[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ double Bs[BLOCK_SIZE][BLOCK_SIZE];

    // Loop over all the sub-matrices of A and B of dimension BLOCK_SIZE * BLOCK_SIZE
    // at offsets determined by blockIdx.{x,y}. Multiply each pair of sub-matrices
    // together and accumulate the results in Cvalue
    for (int i = 0; i < (k / BLOCK_SIZE); ++i)
    {

        // Load Asub and Bsub from device memory to shared memory
        // Each thread loads one element of each sub-matrix
        const int blockStride = i*BLOCK_SIZE;
        As[threadRow][threadCol] = A[(blockStride+threadCol)*m + globRow];
        Bs[threadRow][threadCol] = B[globCol*k + blockStride+threadRow];

        // Synchronize to make sure the sub-matrices are loaded
        // before starting the computation
        __syncthreads();
        // Multiply Asub and Bsub together
        for (int e = 0; e < BLOCK_SIZE; ++e)
            Cvalue += As[threadRow][e]*Bs[e][threadCol];

        // Synchronize to make sure that the preceding computation is done before loading
        // two new sub-matrices of A and B in the next iteration
        __syncthreads();
    }

    // Write Csub to device memory, each thread writes one element
    const int linIdx = globCol*m + globRow;
    C[linIdx] = alpha*Cvalue + beta*C[linIdx];
}

void myDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
  dim3 dimGrid(n/BLOCK_SIZE, m/BLOCK_SIZE);
  dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

  sharedDgemm<<<dimGrid, dimBlock>>>(m, n, k, alpha, A, B, beta, C);
  CUDA_CHECK(cudaGetLastError());
}
