#!/usr/bin/env python3

# In this example, we demonstrate how Korali samples the posterior distribution
# in a bayesian problem where the likelihood is calculated by providing
# reference data points and their objective values.


# Importing the computational model
import sys
sys.path.append('./_model')
from model import *

# Import Korali TODO

# Create Korali engine TODO

# Creating new experiment TODO

# Setting up the reference likelihood for the Bayesian Problem TODO

# Configuring TMCMC parameters
e["Solver"]["Type"] = "Sampler/TMCMC"
e["Solver"]["Population Size"] = 5000
e["Solver"]["Target Coefficient Of Variation"] = 0.5
e["Solver"]["Covariance Scaling"] = 0.04


# Configuring the problem's random distributions TODO

# Configuring the problem's variables and their prior distributions TODO


e["Store Sample Information"] = True


# Configuring output settings TODO


# Set console Verbosity
e["Console Output"]["Verbosity"] = "Detailed"

# Run Experiment TODO
k.run(e)
