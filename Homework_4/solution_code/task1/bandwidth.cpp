#include <fstream>
#include <mpi.h>
#include <stdexcept>
#include <vector>
#include <iostream>

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (size != 2) 
    {
        MPI_Finalize();
        throw std::runtime_error("Need exactly two ranks for this benchmark");
    }

    const size_t iterations = 1000;
    const size_t min_size = 1;        // 1 byte
    const size_t max_size = 1 << 24;  // 16 Mbyte
    std::vector<char> send_buf(max_size);
    std::vector<char> recv_buf(max_size);

    std::ofstream fout;
    if (rank == 0)
    {
        std::string fname("results.dat");
        if (argc == 2)
        {
            fname = argv[1];
        }
        fout.open(fname);
    }
    for (size_t msg_size = min_size; msg_size <= max_size; msg_size *= 2)
    {
        double time = -MPI_Wtime();
        if (rank == 0)
        {
            for (size_t i = 0; i < iterations; ++i)
                MPI_Send(send_buf.data(), msg_size, MPI_CHAR, 1, 100, MPI_COMM_WORLD);
        } 
        else if (rank == 1)
        {
            for (size_t i = 0; i < iterations; ++i)
                MPI_Recv(recv_buf.data(), msg_size, MPI_CHAR, 0, 100, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        MPI_Barrier(MPI_COMM_WORLD);
        time += MPI_Wtime();

        if (rank == 0) 
        {
            const double total_Mbyte = msg_size * iterations / 1.0e6;
            fout << msg_size << '\t' << total_Mbyte / time << '\n';
        }
    }

    MPI_Finalize();
    return 0;
}