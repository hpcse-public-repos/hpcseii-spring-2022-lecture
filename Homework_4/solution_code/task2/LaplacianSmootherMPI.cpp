#include "LaplacianSmootherMPI.h"
using namespace std::chrono;

LaplacianSmootherMPI::LaplacianSmootherMPI(const int Nx,
                                           const int Ny,
                                           const int Nz,
                                           const int Px,
                                           const int Py,
                                           const int Pz,
                                           const MPI_Comm comm_root)
    : LaplacianSmoother(Nx, Ny, Nz), comm_world(comm_root),
      comm_cart(MPI_COMM_NULL), procs{Px, Py, Pz},
      recv_req(6, MPI_REQUEST_NULL), send_req(6, MPI_REQUEST_NULL)
{
    int size;
    MPI_Comm_size(comm_world, &size);
    if (size != Px * Py * Pz)
    {
        MPI_Abort(comm_world, 1);
    }
    const int periodic[3] = {true, true, true};
    MPI_Cart_create(comm_world, 3, procs, periodic, true, &comm_cart);
    MPI_Comm_rank(comm_cart, &rank_cart);
    MPI_Cart_shift(comm_cart, 0, 1, &nbr[X0], &nbr[X1]);
    MPI_Cart_shift(comm_cart, 1, 1, &nbr[Y0], &nbr[Y1]);
    MPI_Cart_shift(comm_cart, 2, 1, &nbr[Z0], &nbr[Z1]);

    // create communication datatype templates
    MPI_Type_vector(Ny, 1, N[0], MPI_DOUBLE, &StripeX);
    MPI_Type_create_hvector(Nz, 1, N[0] * N[1] * sizeof(double), StripeX, &FaceX);
    MPI_Type_vector(Nz, Nx, N[0] * N[1], MPI_DOUBLE, &FaceY);
    MPI_Type_vector(Ny, Nx, N[0], MPI_DOUBLE, &FaceZ);
    MPI_Type_commit(&StripeX);
    MPI_Type_commit(&FaceX);
    MPI_Type_commit(&FaceY);
    MPI_Type_commit(&FaceZ);

    // initialize data
    int coords [3];
    MPI_Cart_coords(comm_cart, rank_cart, 3, coords);
    for (int k = 0; k < Nz; ++k)
    for (int j = 0; j < Ny; ++j)
    for (int i = 0; i < Nx; ++i)
    {
        const int ig = Nx * coords[0] + i;
        const int jg = Ny * coords[1] + j;
        const int kg = Nz * coords[2] + k;
        operator()(i, j, k) = 123*ig*ig + 456*jg*jg + 789*kg*kg;
    }
}

LaplacianSmootherMPI::~LaplacianSmootherMPI()
{
    MPI_Comm_free(&comm_cart);
    MPI_Type_free(&StripeX);
    MPI_Type_free(&FaceX);
    MPI_Type_free(&FaceY);
    MPI_Type_free(&FaceZ);
}

void LaplacianSmootherMPI::report()
{
    int size;
    MPI_Comm_size(comm_cart, &size);
    std::vector<double> v_sweep   (size, 0.0);
    std::vector<double> v_async   (size, 0.0);
    std::vector<double> v_inner   (size, 0.0);
    std::vector<double> v_ghosts  (size, 0.0);
    std::vector<double> v_boundary(size, 0.0);

    MPI_Gather(&t_sweep   , 1, MPI_DOUBLE, v_sweep.data()   , 1, MPI_DOUBLE, 0, comm_cart);
    MPI_Gather(&t_async   , 1, MPI_DOUBLE, v_async.data()   , 1, MPI_DOUBLE, 0, comm_cart);
    MPI_Gather(&t_inner   , 1, MPI_DOUBLE, v_inner.data()   , 1, MPI_DOUBLE, 0, comm_cart);
    MPI_Gather(&t_ghosts  , 1, MPI_DOUBLE, v_ghosts.data()  , 1, MPI_DOUBLE, 0, comm_cart);
    MPI_Gather(&t_boundary, 1, MPI_DOUBLE, v_boundary.data(), 1, MPI_DOUBLE, 0, comm_cart);

    const int Nx = N[0] - 2;
    const int Ny = N[1] - 2;
    const int Nz = N[2] - 2;
    double checksum_global = 0.0;
    double checksum = 0.0;
    for (int k = 0; k < Nz; ++k)
    for (int j = 0; j < Ny; ++j)
    for (int i = 0; i < Nx; ++i)
        checksum += operator()(i,j,k);
    MPI_Reduce(&checksum,&checksum_global,1,MPI_DOUBLE,MPI_SUM,0,comm_cart);

    if (rank_cart == 0)
    {
        // min-max-avg
        // sweep, async, inner, ghosts, boundary
        const std::string header = "sweep\tasync\tinner\tghosts\tboundary\n";
        double mma[5][3] = {{1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0}};
        double *measure[5] = {v_sweep.data(),
                              v_async.data(),
                              v_inner.data(),
                              v_ghosts.data(),
                              v_boundary.data()};

        std::ofstream per_rank("rank_stats_" + std::to_string(procs[0]) +
                                               std::to_string(procs[1]) +
                                               std::to_string(procs[2]) + ".dat");
        per_rank << "# rank\t" << header;
        for (int i = 0; i < size; ++i)
	    {
            per_rank << i;
            for (int j = 0; j < 5; ++j)
	        {
                measure[j][i] /= sweep_count;
                per_rank << '\t' << measure[j][i];
                mma[j][0] = (measure[j][i] < mma[j][0]) ? measure[j][i] : mma[j][0];
                mma[j][1] = (measure[j][i] > mma[j][1]) ? measure[j][i] : mma[j][1];
                mma[j][2] += measure[j][i];
            }
            per_rank << '\n';
        }
        for (int j = 0; j < 5; ++j)
	    {
            mma[j][2] /= size;
        }

        const int Nx = N[0] - 2;
        const int Ny = N[1] - 2;
        const int Nz = N[2] - 2;
        std::printf("Measurement report for dimension %d x %d x %d per rank. Topology %d x %d x %d:\n", Nx, Ny, Nz, procs[0], procs[1], procs[2] );
        std::printf("\tSweep count:           %zu\n", sweep_count);
        std::printf("\tAvg. time sweep:       min:%.3e, max:%.3e, avg:%.3e microseconds\n", mma[0][0], mma[0][1], mma[0][2]);
        std::printf("\tAvg. time async comm:  min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[1][0], mma[1][1], mma[1][2], 100*mma[1][2]/mma[0][2]);
        std::printf("\tAvg. time inner:       min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[2][0], mma[2][1], mma[2][2], 100*mma[2][2]/mma[0][2]);
        std::printf("\tAvg. time sync ghosts: min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[3][0], mma[3][1], mma[3][2], 100*mma[3][2]/mma[0][2]);
        std::printf("\tAvg. time boundary:    min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[4][0], mma[4][1], mma[4][2], 100*mma[4][2]/mma[0][2]);
        std::printf("\tChecksum:    %10.8e \n", checksum_global);
    }
}

void LaplacianSmootherMPI::comm()
{
    const int Nx = N[0] - 2;
    const int Ny = N[1] - 2;
    const int Nz = N[2] - 2;

    // receive of 6 faces directly into ghost buffers
    MPI_Irecv(&operator()(-1, 0, 0), 1, FaceX, nbr[X0], 100, comm_cart, &recv_req[0]);
    MPI_Irecv(&operator()(Nx, 0, 0), 1, FaceX, nbr[X1], 101, comm_cart, &recv_req[1]);
    MPI_Irecv(&operator()(0, -1, 0), 1, FaceY, nbr[Y0], 102, comm_cart, &recv_req[2]);
    MPI_Irecv(&operator()(0, Ny, 0), 1, FaceY, nbr[Y1], 103, comm_cart, &recv_req[3]);
    MPI_Irecv(&operator()(0, 0, -1), 1, FaceZ, nbr[Z0], 104, comm_cart, &recv_req[4]);
    MPI_Irecv(&operator()(0, 0, Nz), 1, FaceZ, nbr[Z1], 105, comm_cart, &recv_req[5]);

    // send of 6 faces from internal domain
    MPI_Isend(&operator()(0, 0, 0),    1, FaceX, nbr[X0], 101, comm_cart, &send_req[0]);
    MPI_Isend(&operator()(Nx-1, 0, 0), 1, FaceX, nbr[X1], 100, comm_cart, &send_req[1]);
    MPI_Isend(&operator()(0, 0, 0),    1, FaceY, nbr[Y0], 103, comm_cart, &send_req[2]);
    MPI_Isend(&operator()(0, Ny-1, 0), 1, FaceY, nbr[Y1], 102, comm_cart, &send_req[3]);
    MPI_Isend(&operator()(0, 0, 0),    1, FaceZ, nbr[Z0], 105, comm_cart, &send_req[4]);
    MPI_Isend(&operator()(0, 0, Nz-1), 1, FaceZ, nbr[Z1], 104, comm_cart, &send_req[5]);
}

void LaplacianSmootherMPI::sync()
{
    // wait for pending sends (because we will write to it)
    MPI_Waitall(6, send_req.data(), MPI_STATUSES_IGNORE);

    // wait for pending recvs (because we will read from it)
    MPI_Waitall(6, recv_req.data(), MPI_STATUSES_IGNORE);
}