#include <cstdio>
#include <cstdlib>
#include <vector>

__global__ void gemvGPU1(const double *m, const double *v, double *w, int N) {
    // TODO: Subquestion (a). Naive translation of gemvCPU.
}

__global__ void gemvGPU2(const double *m, const double *v, double *w, int N) {
    // TODO: Subquestion (b). Utilize shared memory to speed up the kernel.
}

void gemvCPU(const double *m, const double *v, double *w, int N) {
    for (int i = 0; i < N; ++i) {
        double sum = 0.0;
        for (int j = 0; j < N; ++j)
            sum += m[j * N + i] * v[j];
        w[i] = sum;
    }
}


void test(int N) {
    double *mHost;
    double *vHost;
    double *wHost;
    double *mDev;
    double *vDev;
    double *wDev;

    cudaMallocHost(&mHost, N * N * sizeof(double));
    cudaMallocHost(&vHost, N * sizeof(double));
    cudaMallocHost(&wHost, N * sizeof(double));
    cudaMalloc(&mDev, N * N * sizeof(double));
    cudaMalloc(&vDev, N * sizeof(double));
    cudaMalloc(&wDev, N * sizeof(double));

    for (int j = 0; j < N; ++j)
        for (int i = 0; i < N; ++i)
            mHost[j * N + i] = (double)(rand() % 10);
    for (int i = 0; i < N; ++i)
        vHost[i] = (double)(rand() % 10);

    std::vector<double> wExpected(N);
    gemvCPU(mHost, vHost, wExpected.data(), N);

    cudaMemcpy(mDev, mHost, N * N * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(vDev, vHost, N * sizeof(double), cudaMemcpyHostToDevice);

    // TODO: Launch the first or the second kernel to verify if its correctness.


    cudaMemcpy(wHost, wDev, N * sizeof(double), cudaMemcpyDeviceToHost);

    bool error = false;
    for (int i = 0; i < N; ++i)
        error |= wHost[i] != wExpected[i];

    if (error) {
        printf("Incorrect result for N=%d\n", N);
        printf(" idx  expected  computed\n");
        for (int i = 0; i < N; ++i) {
            printf("%4d %9.2f %9.2f%s\n", i, wExpected[i], wHost[i],
                   wHost[i] != wExpected[i] ? " <------- incorrect" : "");
        }
        exit(1);
    }

    cudaFree(wDev);
    cudaFree(vDev);
    cudaFree(mDev);
    cudaFreeHost(wHost);
    cudaFreeHost(vHost);
    cudaFreeHost(mHost);

    printf("N=%d OK!\n", N);
}

int main() {
    test(1);
    test(5);
    test(15);
    test(130);
    test(260);
    test(1000);
    test(5000);
}
