// Skeleton code
// MPI Load-Balancing

#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <mpi.h>
#include <vector>

constexpr int L = 400;

struct Block {
    double u[L];
    int N[2];

    // Initialize with some values.
    Block()
    {
        int r;
        MPI_Comm_rank(MPI_COMM_WORLD, &r);
        for (int j = 0; j < L; ++j) {
            u[j] = 0.12345 * r * r + j;
        }
        N[0] = 10 * r;
        N[1] = r;
    }

    // Do some flops, to simulate a unit workload w^n_{i,j}=1.
    void work()
    {
        for (int j = 0; j < L; ++j) {
            u[j] = std::pow(u[j] + N[0], 0.1 * N[1]);
        }
    }
};

struct LoadBalancer {
    MPI_Comm grid;             // Cartesian communicator.
    int size;                  // Total number of processes.
    int rank;                  // This rank.
    int up, down, left, right; // Neighboring ranks.
    int coords[2];             // Index space coordinates of this rank.
    std::vector<Block> blocks; // This rank's blocks.
    MPI_Datatype MPI_BLOCK;

    LoadBalancer(int initialBlocks)
    {
        defineCommunicator(); // QUESTION 1.
        defineDatatype();     // QUESTION 2.
        blocks.resize(initialBlocks);
    }

    void defineCommunicator()
    {
        // QUESTION 1:
        // Define a Cartesian communicator and create an M x M periodic grid
        // of processes. Also determine processes up(i+1,j), down(i-1,j),
        // left(i,j-1) and right(i,j+1), as well as indices (i,j) and rank ID
        // of this process.
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        int M = (int)(std::sqrt(size) + 0.5);
        if (M * M != size) {
            std::cout
                << "Number of processes must be a square number. Aborting...\n";
            int err_code = 1;
            MPI_Abort(MPI_COMM_WORLD, err_code);
        }

        /**********************************/
        /* start of answer for question 1 */
        /**********************************/
        // TODO:

        // These two lines should be replaced ('grid' should be the Cartesian
        // communicator and 'rank' the process ID in that communicator).
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        grid = MPI_COMM_WORLD;
        /**********************************/
        /* end of answer for question 1   */
        /**********************************/
    }

    void defineDatatype()
    {
        // Question 2:
        // Define a custom MPI Datatype to send/receive struct Block between
        // processes.

        /**********************************/
        /* start of answer for question 2 */
        /**********************************/
        // TODO:

        /**********************************/
        /* end of answer for question 2   */
        /**********************************/
    }

    void diffuseWork()
    {
        // 1. Find out how many blocks your neighbors have and let them know
        // about your blocks.
        int myBlocks = blocks.size();
        int upBlocks = myBlocks;
        int downBlocks = myBlocks;
        int leftBlocks = myBlocks;
        int rightBlocks = myBlocks;

        // upBlocks, downBlocks, leftBlocks and rightBlocks are initialized to
        // the value of myBlocks but this needs to change:
        // Here you should acquire the values of upBlocks, downBlocks,
        // leftBlocks, rightBlocks from the number of the blocks of ranks
        // (i+1,j), (i-1,j), (i,j+1), (i,j-1)
        /************************************************/
        /* start of answer for question 3 (part 1 of 2) */
        /************************************************/
        // TODO:

        /************************************************/
        /* end of answer for question 3 (part 1 of 2)   */
        /************************************************/

        // 2. Determine how many blocks will be sent/received to/from each
        // neighbor.
        double lambda = 8.0;
        int fluxUp = (myBlocks - upBlocks) / lambda; // Round-to-zero division.
        int fluxDown = (myBlocks - downBlocks) / lambda;
        int fluxRight = (myBlocks - rightBlocks) / lambda;
        int fluxLeft = (myBlocks - leftBlocks) / lambda;

        // 3. Allocate memory to receive blocks and copy blocks to be sent.
        int fluxes[4] = {fluxUp, fluxDown, fluxRight, fluxLeft};
        std::array<std::vector<Block>, 4> blocksReceived;
        std::array<std::vector<Block>, 4> blocksSent;
        for (int j = 0; j < 4; ++j) {
            if (fluxes[j] > 0) {
                // Move blocks from blocks to blocksSent.
                blocksSent[j].reserve(fluxes[j]);
                for (int k = 0; k < fluxes[j]; ++k) {
                    blocksSent[j].push_back(blocks.back());
                    blocks.pop_back();
                }
            } else if (fluxes[j] < 0) {
                blocksReceived[j].resize(-fluxes[j]);
            }
        }

        // 4. Send and receive the blocks.
        // You should send/receive blocks that are stored in blocksSent and
        // blocksReceived by using the custom datatype MPI_BLOCK.
        // NOTE: the number of blocks to send/receive is equal to
        // blocksSent[i].size()/blocksReceived[i].size(), i=0,1,2,3
        /************************************************/
        /* start of answer for question 3 (part 2 of 2) */
        /************************************************/
        // TODO:

        /************************************************/
        /* end of answer for question 3 (part 2 of 2)   */
        /************************************************/

        // 5. Copy received blocks.
        for (int j = 0; j < 4; ++j) {
            if (fluxes[j] < 0) {
                for (int k = 0; k < -fluxes[j]; ++k) {
                    blocks.push_back(blocksReceived[j][k]);
                }
            }
        }
    }
};

// Do not change code below here
////////////////////////////////////////////////////////////////////////////////
void changeLoad(std::vector<Block> &blocks, int rank, int size)
{
    srand(rank);
    double temp = (double)rank / (double)size;
    double prob = std::min(temp * temp * 5., 1.0);
    double r = (double)rand() / RAND_MAX;
    if (r > prob) { // Generate more blocks.
        int newBlocks = (int)(1000 * (r - prob));
        for (int j = 0; j < newBlocks; ++j) {
            blocks.push_back(Block());
        }
    }
}

double displayDistribution(int rank, int size, int blocks, MPI_Comm comm)
{
    double ratio = 1.0;

    std::vector<int> b(size);
    b[rank] = blocks;

    MPI_Gather(&blocks, 1, MPI_INT, &b[0], 1, MPI_INT, 0, comm);
    if (rank == 0) {
        std::cout << "Block distribution:\n";
        int s = (int)(std::sqrt(size) + 0.5);
        int m0 = 0;
        int m1 = b[0];
        for (int i0 = 0; i0 < s; ++i0) {
            std::cout << '\t';
            for (int i1 = 0; i1 < s; ++i1) {
                m0 += b[i0 + i1 * s];
                m1 = std::max(b[i0 + i1 * s], m1);
                std::cout << b[i0 + i1 * s] << ' ';
            }
            std::cout << '\n';
        }
        ratio = s * s * (double)m1 / (double)m0;
    }
    return ratio;
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    LoadBalancer manager(100 *
                         size); // Each rank starts with the same workload:
                                // w^0_{i,j} = 100*size for all i,j.
    double ratio = 1.0;         // Initial load-imbalance ratio = 1.0.

    double totalTime = 0.0;
    int maxIterations = 20;
    for (int j = 0; j < maxIterations; ++j) {
        if (rank == 0) {
            std::cout << "Iteration:" << j
                      << " load-imbalance ratio = " << ratio << '\n';
        }

        MPI_Barrier(MPI_COMM_WORLD);
        double t1 = MPI_Wtime();

        if (j % 10 == 0) {
            changeLoad(manager.blocks, manager.rank, manager.size);
        }

        // Load balancing among ranks.
        manager.diffuseWork();

        // Perform computation
        for (auto b : manager.blocks) {
            b.work();
        }

        double t2 = MPI_Wtime();
        totalTime += t2 - t1;

        // This will display distribution of blocks among ranks and update the
        // load-imbalance ratio.
        ratio = displayDistribution(
            manager.rank, manager.size, manager.blocks.size(), manager.grid);
    }

    double maxTime;
    MPI_Reduce(&totalTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        std::cout << "Total time = " << maxTime << '\n';
    }

    double partialChecksum = 0.0;
    for (int j = 0; j < (int)manager.blocks.size(); ++j) {
        for (int k = 0; k < L; ++k) {
            partialChecksum += manager.blocks[j].u[k];
        }
    }
    double checksum;
    MPI_Reduce(
        &partialChecksum, &checksum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        std::cout << "checksum = " << checksum << '\n';
    }

    MPI_Finalize();
    return 0;
}
