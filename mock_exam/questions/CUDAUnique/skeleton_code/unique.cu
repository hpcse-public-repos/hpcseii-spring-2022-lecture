#include <algorithm>
#include <cstdio>
#include <cstdlib>

// TODO: Subquestion (a): Implement building blocks.

__device__ int computeWarpInscan(int x) {
    // Compute the inscan of values `x` within the current warp.
    return 0;
}

__device__ int computeBlockInscan(int x) {
    // Compute the inscan of values `x` within the current block.
    return 0;
}


// TODO: Subquestion (d): Optimize for bools.

__device__ int computeWarpInscan(bool x) {
    return computeWarpInscan((int)x);
}

__device__ int computeBlockInscan(bool x) {
    return computeBlockInscan((int)x);
}


// Subquestion (b)

__global__ void blockUniqueKernel(const int *a, int *u, int *L, int N) {
    // TODO: Implement unique operation for N <= 1024.
}


// Subquestion (c)

__global__ void computeBlockCountsKernel(const int *a, int *cnts, int N) {
    // TODO: Compute how many numbers from each block will be stored in the final array.
}

__global__ void computeBlockOffsetsKernel(const int *cnts, int *offsets, int numBlocks) {
    // TODO: Compute where the output of each block starts, compared to the
    // complete output array u.
}


__global__ void uniqueKernel(const int *a, int *u, int *L, int N, const int *offsets) {
    // TODO: Implement copying of unique values to the output array.
}


void computeUniqueValues(const int *a, int *u, int *L, int N) {
    // Assume:
    //      a is a device array of size N
    //      u is a device array of size N
    //      L is a device pointer to an int
    //      N <= 1024^2

    if (N <= 1024) {
        // TODO: (b) Launch the blockUniqueKernel.
    } else {
        // TODO: (c) Allocate temporary arrays.

        // TODO: (c) Launch the three kernels for performing the unique operation.

        // TODO: (c) Deallocate temporary arrays.
    }
}

void test(int N) {
    int *aHost;
    int *uHost;
    int *aDev;
    int *uDev;
    cudaMallocHost(&aHost, N * sizeof(int));
    cudaMallocHost(&uHost, (N + 1) * sizeof(int));  // +1 for L
    cudaMalloc(&aDev, N * sizeof(int));
    cudaMalloc(&uDev, (N + 1) * sizeof(int));  // +1 for L

    for (int i = 0; i < N; ++i)
        aHost[i] = 1 + rand() % 3;

    cudaMemcpy(aDev, aHost, N * sizeof(int), cudaMemcpyHostToDevice);

    computeUniqueValues(aDev, uDev, uDev + N, N);
    cudaMemcpy(uHost, uDev, (N + 1) * sizeof(int), cudaMemcpyDeviceToHost);

    int L = std::unique(aHost, aHost + N) - aHost;

    bool error = uHost[N] != L;
    for (int i = 0; i < L && !error; ++i)
        if (uHost[i] != aHost[i])
            error = true;

    if (error) {
        printf("Failed for N=%d! Expected L=%d computed=%d.\n", N, L, uHost[N]);
        printf(" idx  expected  computed\n");
        for (int i = 0; i < L; ++i) {
            printf("%4d  %8d  %8d%s\n", i, aHost[i], uHost[i],
                   aHost[i] != uHost[i] ? " <-------- incorrect" : "");
        }
        exit(1);
    }


    cudaFree(uDev);
    cudaFree(aDev);
    cudaFreeHost(uHost);
    cudaFreeHost(aHost);

    printf("N=%d OK!\n", N);
}


int main() {
    test(32);
    test(64);
    test(1000);
    test(1024);
    test(1050);
    test(1000000);
    test(1024 * 1024);
}
