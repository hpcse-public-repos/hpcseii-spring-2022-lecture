#include <cstdio>

struct /* alignas(8) */ Vector2f {
    float x;
    float y;
};

using T = Vector2f;
// using T = float2;

__global__ void kernel(T *a, const T *b, const T *c, int N) {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
         i < N;
         i += blockDim.x * gridDim.x) {
        a[i].x = b[i].x * a[i].x + c[i].x;
        a[i].y = b[i].y * a[i].y + c[i].y;
    }
}

int main() {
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int N = 200000000;
    T *aDev;
    T *bDev;
    T *cDev;
    cudaMalloc(&aDev, N * sizeof(T));
    cudaMalloc(&bDev, N * sizeof(T));
    cudaMalloc(&cDev, N * sizeof(T));

    cudaEventRecord(start);
    // 56 matches number of SMs on P100
    kernel<<<56, 1024>>>(aDev, bDev, cDev, N);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    double seconds = 0.001 * milliseconds;

    double flops = 4 * N;
    printf("time: %.3f s   performance: %.2f GFLOP/s\n", seconds, flops / seconds / 1e9);

    cudaEventDestroy(stop);
    cudaEventDestroy(start);
}

