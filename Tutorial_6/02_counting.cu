#include <cstdio>
#include <algorithm>

__global__ void countOccurrences(const int *in, int *counters, int N) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < N)
        atomicAdd(&counters[in[idx]], 1);
}

/// Optimized for frequent repeating values in `in[]`.
__global__ void countOccurrencesFast(const int *in, int *counters, int N) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int value = idx < N ? in[idx] : -1;

    unsigned mask = __match_any_sync(0xFFFFFFFF, value);
    int laneIdx = threadIdx.x & 31;
    bool isLeader = ((mask & -mask) >> laneIdx) & 1 != 0;
    if (isLeader && idx < N)
        atomicAdd(&counters[value], __popc(mask));
}

__global__ void compare(const int *cnt1, const int *cnt2, int K) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < K && cnt1[idx] != cnt2[idx]) {
        printf("error: idx=%d  cnt1[idx]=%d  cnt2[idx]=%d\n",
               idx, cnt1[idx], cnt2[idx]);
    }
}

int main() {
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int N = 10000000;
	int K = 1000;
    int *inHost;
    int *inDev;
    int *cnt1Dev;
    int *cnt2Dev;
    cudaMallocHost(&inHost, N * sizeof(int));
    cudaMalloc(&inDev, N * sizeof(int));
    cudaMalloc(&cnt1Dev, K * sizeof(int));
    cudaMalloc(&cnt2Dev, K * sizeof(int));

    for (int i = 0; i < N; ++i)
        inHost[i] = rand() % K;
    std::sort(inHost, inHost + N);

    cudaMemcpy(inDev, inHost, N * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemset(cnt1Dev, 0, K * sizeof(int));
    cudaMemset(cnt2Dev, 0, K * sizeof(int));

    int threadsPerBlock = 256;
    int numBlocks = (N + threadsPerBlock - 1) / threadsPerBlock;

    countOccurrences<<<numBlocks, threadsPerBlock>>>(inHost, cnt1Dev, N);
    countOccurrencesFast<<<numBlocks, threadsPerBlock>>>(inHost, cnt2Dev, N);

    cudaEventRecord(start);
    countOccurrences<<<numBlocks, threadsPerBlock>>>(inHost, cnt1Dev, N);
    countOccurrences<<<numBlocks, threadsPerBlock>>>(inHost, cnt1Dev, N);
    countOccurrences<<<numBlocks, threadsPerBlock>>>(inHost, cnt1Dev, N);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds1 = 0;
    cudaEventElapsedTime(&milliseconds1, start, stop);

    cudaEventRecord(start);
    countOccurrencesFast<<<numBlocks, threadsPerBlock>>>(inHost, cnt2Dev, N);
    countOccurrencesFast<<<numBlocks, threadsPerBlock>>>(inHost, cnt2Dev, N);
    countOccurrencesFast<<<numBlocks, threadsPerBlock>>>(inHost, cnt2Dev, N);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds2 = 0;
    cudaEventElapsedTime(&milliseconds2, start, stop);

    compare<<<(K + threadsPerBlock - 1) / K, threadsPerBlock>>>(cnt1Dev, cnt2Dev, K);
    cudaDeviceSynchronize();

    printf("unoptimized kernel: %.3f ms\n", milliseconds1);
    printf("optimized kernel:   %.3f ms\n", milliseconds2);

    cudaEventDestroy(stop);
    cudaEventDestroy(start);
}
