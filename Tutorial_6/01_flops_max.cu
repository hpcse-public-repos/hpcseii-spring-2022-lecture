#include <cstdio>
#include <cstdlib>

__global__ void kernel(double a, double b, double x, int N, double *out) {
    for (int i = 0; i < N; ++i) {
        x = a * x + b;
    }
    *out = x;
}

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Usage: ./flops_max numBlocks threadsPerBlock\n");
        return 1;
    }
    int numBlocks = std::atoi(argv[1]);
    int threadsPerBlock = std::atoi(argv[2]);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    double *outDev;
    cudaMalloc(&outDev, 1 * sizeof(double));

    int N = 10000000;
    double a = 1.0000000001;
    double b = 1.0123;
    double x = 123.0;

    cudaEventRecord(start);
    kernel<<<numBlocks, threadsPerBlock>>>(a, b, x, N, outDev);
    int code = cudaGetLastError();
    if (code) {
        fprintf(stderr, "ERROR: %d\n", code);
        return code;
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    double seconds = 0.001 * milliseconds;

    double flops = (double)2 * N * numBlocks * threadsPerBlock;
    printf("time: %.3f s   performance: %.2f GLOP/s\n", seconds, flops / seconds / 1e9);

    cudaEventDestroy(stop);
    cudaEventDestroy(start);
}
