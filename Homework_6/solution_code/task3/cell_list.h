#pragma once

#include "scan.h"

struct Interaction;

/// Cell list metadata used from various kernels.
/// (useful both in cell lists building and in force computation)
struct CellListInfo {
    /// Inverse cell size.
    double2 invCellSize;

    /// Number of cells for each dimension.
    int2 numCells;

    /// Pointer to the offsets array.
    int *offsets;

    /// Return the index pair (ix, iy) of the cell containing the given point.
    __device__ int2 getCell(double2 p) const {
        const int cix = (int)(p.x * invCellSize.x);
        const int ciy = (int)(p.y * invCellSize.y);
        return {cix, ciy};
    }

    /// Return the row-major index of the cell containing the given point.
    __device__ int getCellIndex(double2 p) const {
        const int2 c = getCell(p);
        const int cIdx = c.y * numCells.x + c.x;
        return cIdx;
    }
};

class CellList {
public:
    /// Create cell list that spans across the given domain with given cell size.
    CellList(double2 domainSize, double cellSize);
    ~CellList();

    /// Build cell list for given particles and reorder the particle positions.
    void build(const double2 *pDev, double2 *pSortedDev, int numParticles);

    /// Return the cell list info / metadata.
    CellListInfo getInfo() const;
private:
    Scan scan_;

    double2 invCellSize_;
    int2 numCells_;
    int *countsDev_ = nullptr;
    int *offsetsDev_ = nullptr;
};
