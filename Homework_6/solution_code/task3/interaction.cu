#include "cell_list.h"
#include "interaction.h"
#include "../include/utils.h"

__global__ void computeForcesKernel(
        CellListInfo info,
        Interaction interaction,
        const double2 *p,
        double2 *f,
        int numParticles) {
    // In principle, the grid-strided for loop is not necessary here since a
    // separate thread is already launched for each particle.
    for (int idx = blockIdx.x * blockDim.x + threadIdx.x;
             idx < numParticles;
             idx += gridDim.x * blockDim.x) {
        // Compute which cell the particle belongs to.
        const int2 c = info.getCell(p[idx]);

        // Compute the neighborhood, taking into account the domain bounds.
        const int cMinX = max(c.x - 1, 0);
        const int cMinY = max(c.y - 1, 0);
        const int cMaxX = min(c.x + 1, info.numCells.x - 1);
        const int cMaxY = min(c.y + 1, info.numCells.y - 1);

        const double2 myPos = p[idx];
        double2 fTotal{0.0, 0.0};

        // Iterate over up to 3 rows of cells.
        for (int cY = cMinY; cY <= cMaxY; ++cY) {
            // Compute the part of the (sorted) particle array that represents
            // particles belonging to the current row of cells (up to 3 cells).
            const int cIdxMin = cY * info.numCells.x + cMinX;
            const int cIdxMax = cY * info.numCells.x + cMaxX;
            const int pIdxBegin = info.offsets[cIdxMin];
            const int pIdxEnd   = info.offsets[cIdxMax + 1];

#pragma unroll 4
            for (int pIdx = pIdxBegin; pIdx < pIdxEnd; ++pIdx) {
                // For idx == pIdx, the resulting force will be (0, 0), as long
                // as the interaction alpha is greater than 0, so no need to
                // skip this iteration manually.
                const double2 fCurrent = interaction(myPos, p[pIdx]);
                fTotal.x += fCurrent.x;
                fTotal.y += fCurrent.y;
            }
        }
        f[idx] = fTotal;
    }
}

void computeForces(
        const CellListInfo info,
        Interaction interaction,
        const double2 *pSortedDev,
        double2 *f,
        int numParticles) {
    const int threads = 256;
    const int blocks = (numParticles + threads - 1) / threads;
    CUDA_LAUNCH(computeForcesKernel, blocks, threads,
                info, interaction, pSortedDev, f, numParticles);
}
