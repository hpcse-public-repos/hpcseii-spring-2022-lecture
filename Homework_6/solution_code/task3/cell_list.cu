#include "cell_list.h"
#include "interaction.h"
#include "../include/utils.h"

__global__ void countParticlePerCell(
        CellListInfo info, int *counts, const double2 *p, int numParticles) {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
             i < numParticles;
             i += gridDim.x * blockDim.x) {
        const int cIdx = info.getCellIndex(p[i]);
        atomicAdd(&counts[cIdx], 1);
    }
}

__global__ void rearrangeParticles(
        CellListInfo info,
        int *counts,
        const double2 *p,
        double2 *pNew,
        int numParticles) {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
             i < numParticles;
             i += gridDim.x * blockDim.x) {
        const int cIdx = info.getCellIndex(p[i]);
        const int newIndex = info.offsets[cIdx] + atomicAdd(&counts[cIdx], 1);
        pNew[newIndex] = p[i];
    }
}

CellList::CellList(double2 domainSize, double cellSize) {
    numCells_.x = (int)std::ceil(domainSize.x / cellSize);
    numCells_.y = (int)std::ceil(domainSize.y / cellSize);
    invCellSize_.x = numCells_.x / domainSize.x;
    invCellSize_.y = numCells_.y / domainSize.y;

    const int totalCells = numCells_.x * numCells_.y;
    CUDA_CHECK(cudaMalloc(&countsDev_, totalCells * sizeof(int)));
    CUDA_CHECK(cudaMalloc(&offsetsDev_, (totalCells + 1) * sizeof(int)));
    // Set offsets[0] to 0.
    CUDA_CHECK(cudaMemset(offsetsDev_, 0, 1 * sizeof(int)));
}

CellList::~CellList() {
    CUDA_CHECK(cudaFree(offsetsDev_));
    CUDA_CHECK(cudaFree(countsDev_));
}

/*
__global__ void printInfo(CellListInfo info, const int *counts) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < info.numCells.x * info.numCells.y) {
        printf("idx=%d counts=%d offsets=%d\n",
               idx, counts[idx], info.offsets[idx]);
    }
}
*/

void CellList::build(const double2 *pDev, double2 *pSortedDev, int numParticles) {
    const int threads = 256;
    const int blocks = (numParticles + threads - 1) / threads;
    const CellListInfo info = getInfo();

    // Stage 1: compute cell sizes
    CUDA_CHECK(cudaMemset(countsDev_, 0, numCells_.x * numCells_.y * sizeof(int)));
    CUDA_LAUNCH(countParticlePerCell, blocks, threads, info, countsDev_, pDev, numParticles);

    // Stage 2: compute offsets
    scan_.inclusiveSum(countsDev_, offsetsDev_ + 1, numCells_.x * numCells_.y);
    // Note: it might be beneficial to shift countsDev_ by 1 element and use
    //   scan_.inclusiveSum(countsDev_, offsetsDev_, numCells_.x * numCells_.y + 1);
    // such that the both pointers are aligned to a large power of two
    // (cudaMalloc returns aligned memory addresses).

    // Stage 3: reorder particles into cells
    CUDA_CHECK(cudaMemset(countsDev_, 0, numCells_.x * numCells_.y * sizeof(int)));
    CUDA_LAUNCH(rearrangeParticles, blocks, threads,
                info, countsDev_, pDev, pSortedDev, numParticles);
}

CellListInfo CellList::getInfo() const {
    return {invCellSize_, numCells_, offsetsDev_};
}
