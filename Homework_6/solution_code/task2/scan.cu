#include "scan.h"
#include "../include/utils.h"
#include <algorithm>
#include <cstdio>

using T = int;  // To differentiate between index `int` and value `int`.

Scan::Scan() { }

Scan::~Scan() {
    CUDA_CHECK(cudaFree(tmpDev_));
}

void Scan::inclusiveSum(const int *inDev, int *outDev, int N) {
    if (N <= 1024 * 1024)
        _inclusiveSumABC(inDev, outDev, N);
    else
        _inclusiveSumD(inDev, outDev, N);
}


// Solution for (a), (b) and (c) parts.
namespace solution_abc {

__device__ T computeWarpInscan(T x) {
    const int laneIdx = threadIdx.x & 31;
    T left;
    left = __shfl_up_sync(0xFFFFFFFF, x, 1);
    if (laneIdx >= 1)
        x += left;
    left = __shfl_up_sync(0xFFFFFFFF, x, 2);
    if (laneIdx >= 2)
        x += left;
    left = __shfl_up_sync(0xFFFFFFFF, x, 4);
    if (laneIdx >= 4)
        x += left;
    left = __shfl_up_sync(0xFFFFFFFF, x, 8);
    if (laneIdx >= 8)
        x += left;
    left = __shfl_up_sync(0xFFFFFFFF, x, 16);
    if (laneIdx >= 16)
        x += left;
    return x;
}

static __global__ void blockInscan(const T *in, T *out, T *blockSums, int N) {
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;

    // Compute inscan within a warp. Take care of the case where N % 1024 != 0.
    const T value = idx < N ? in[idx] : T{};
    const T warpInscan = computeWarpInscan(value);

    // Exchange warp sums.
    __shared__ T warpSums[32];
    const int warpIdx = threadIdx.x >> 5;
    if ((threadIdx.x & 31) == 31)
        warpSums[warpIdx] = warpInscan;
    __syncthreads();

    // Compute inscan of warp sums.
    __shared__ T inscannedWarpSums[32];
    if (threadIdx.x < 32) {
        inscannedWarpSums[threadIdx.x] = computeWarpInscan(
                warpSums[threadIdx.x]);
        if (threadIdx.x == 31 && blockSums != nullptr) {
            blockSums[blockIdx.x] = inscannedWarpSums[threadIdx.x];
        }
    }
    __syncthreads();

    // Include the exscan (exscan == inscan - current) of warp sums.
    if (idx < N)
        out[idx] = inscannedWarpSums[warpIdx] - warpSums[warpIdx] + warpInscan;
}

static __global__ void addInscannedBlockSums(const T *inscannedBlocks, T *inOut, int N) {
    // With one-element-per-thread approach, addInscannedBlockSums runs at 365GB/s on V100.
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < N)
        inOut[idx] += inscannedBlocks[idx >> 10];
}

}  // solution_abc

void Scan::_inclusiveSumABC(const T *inDev, T *outDev, int N) {
    using namespace solution_abc;
    const int numBlocks = (N + 1024 - 1) / 1024;

    if (numBlocks > tmpSize_) {
        CUDA_CHECK(cudaFree(tmpDev_));
        // First `numBlocks` elements are block sums, second `numBlocks`
        // elements are the inscan of block sums.
        CUDA_CHECK(cudaMalloc(&tmpDev_, 2 * numBlocks * sizeof(T)));
        tmpSize_ = numBlocks;
    }

    // Block-level inscan, store block sums in tmpDev.
    CUDA_LAUNCH(blockInscan, numBlocks, 1024, inDev, outDev, tmpDev_, N);
    if (numBlocks > 1) {
        CUDA_LAUNCH(blockInscan, 1, 1024, tmpDev_, tmpDev_ + numBlocks, nullptr, numBlocks);

        // First block doesn't need to be updated!
        CUDA_LAUNCH(addInscannedBlockSums, (numBlocks - 1), 1024,
                    tmpDev_ + numBlocks, outDev + 1024, N - 1024);
    }
}


namespace solution_d {

// This solution below works also with num threads / block different than 1024.
// Moreover, the kernel handles more elements per thread using a strided for
// loop.
// Note: the following code is not the most efficient nor the most elegant way
// to compute inscan!

constexpr int kThreads = 1024;

/// Compute the inscan among the first kPartialWarpSize lanes of the warp.
/// Value kPartialWarpSize must be a power of two.
template <int kPartialWarpSize>
__device__ T computePartialWarpInscan(T x) {
    constexpr unsigned mask = (unsigned)((1ULL << kPartialWarpSize) - 1);
    const int laneIdx = threadIdx.x & 31;
    for (int jump = 1; jump < kPartialWarpSize; jump <<= 1)  {
        T left = __shfl_up_sync(mask, x, jump);
        if (laneIdx >= jump)
            x += left;
    }
    return x;
}

static __global__ void blockInscan(const T *in, T *out, T *blockSums, int N, int elementsPerBlock) {
    __shared__ T partsSum;
    if (threadIdx.x == 0)
        partsSum = T{};

    for (int partOffset = 0; partOffset < elementsPerBlock; partOffset += kThreads) {
        const int idx = blockIdx.x * elementsPerBlock + partOffset + threadIdx.x;

        // Compute inscan within a warp.
        // Take care of the case where N % elementsPerBlock != 0.
        const T value = idx < N ? in[idx] : T{};
        const T warpInscan = computePartialWarpInscan<32>(value);

        // Exchange warp sums.
        constexpr int kNumWarps = kThreads >> 5;
        __shared__ T warpSums[kNumWarps];
        const int warpIdx = threadIdx.x >> 5;
        if ((threadIdx.x & 31) == 31)
            warpSums[warpIdx] = warpInscan;
        const T currPartSum = partsSum;
        __syncthreads();

        __shared__ T inscannedWarpSums[kNumWarps];
        if (threadIdx.x < kNumWarps) {
            inscannedWarpSums[threadIdx.x] = computePartialWarpInscan<kNumWarps>(
                    warpSums[threadIdx.x]);
            if (threadIdx.x == kNumWarps - 1)
                partsSum += inscannedWarpSums[threadIdx.x];
        }
        __syncthreads();

        if (idx < N)
            out[idx] = currPartSum + inscannedWarpSums[warpIdx] - warpSums[warpIdx] + warpInscan;
    }
    if (threadIdx.x == kThreads - 1 && blockSums != nullptr)
        blockSums[blockIdx.x] = partsSum;
}

static __global__ void addInscannedBlockSums(const T *inscannedBlocks, T *inOut, int N, int logElementsPerBlock) {
    /*
    // With one-element-per-thread approach, addInscannedBlockSums runs at 365GB/s on V100.
    const int idx = blockIdx.x * kThreads + threadIdx.x;
    if (idx < N) {
        inOut[idx] += inscannedBlocks[idx >> logElementsPerBlock];
    }
    */
    // With multiple-elements-per-thread approach, addInscannedBlockSums runs at 380GB/s on V100.
    for (int idx = blockIdx.x * blockDim.x + threadIdx.x;
             idx < N;
             idx += gridDim.x * blockDim.x) {
        inOut[idx] += inscannedBlocks[idx >> logElementsPerBlock];
    }
}

static int intLog2(int x) {
    int i;
    for (i = 0; (1 << i) < x; ++i)
        continue;
    return i;
}
static int ceilPow2(int x) {
    int i;
    for (i = 1; i < x; i *= 2)
        continue;
    return i;
}

}  // namespace solution_d

void Scan::_inclusiveSumD(const T *inDev, T *outDev, int N) {
    using namespace solution_d;

    // const int blocks = (N + kThreads - 1) / kThreads;
    const int blocks = std::min(kThreads, (N + kThreads - 1) / kThreads);
    const int elementsPerBlock = ceilPow2((N + blocks - 1) / blocks);
    const int logElementsPerBlock = intLog2(elementsPerBlock);

    if (blocks > tmpSize_) {
        CUDA_CHECK(cudaFree(tmpDev_));
        CUDA_CHECK(cudaMalloc(&tmpDev_, 2 * blocks * sizeof(T)));
        tmpSize_ = blocks;
    }

    CUDA_LAUNCH(blockInscan, blocks, kThreads, inDev, outDev, tmpDev_, N, elementsPerBlock);
    if (blocks > 1) {
        CUDA_LAUNCH(blockInscan, 1, kThreads, tmpDev_, tmpDev_ + blocks, nullptr, blocks, kThreads);

        // First block doesn't need to be updated!

        // Multiple elements per thread is slightly faster than one element per thread.
        // CUDA_LAUNCH(addInscannedBlockSums, (blocks - 1) * (elementsPerBlock / kThreads), kThreads,
        //             tmpDev_ + blocks, outDev + elementsPerBlock, N - elementsPerBlock, logElementsPerBlock);
        CUDA_LAUNCH(addInscannedBlockSums, (blocks - 1), kThreads,
                    tmpDev_ + blocks, outDev + elementsPerBlock,
                    N - elementsPerBlock, logElementsPerBlock);
    }
}
