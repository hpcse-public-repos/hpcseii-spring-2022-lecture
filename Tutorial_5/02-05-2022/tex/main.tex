%Please use LuaLaTeX or XeLaTeX
\documentclass[11pt,aspectratio=169]{beamer}
\usepackage{minted}
\usepackage{subcaption}
\usepackage{siunitx}
\setbeamerfont{footnote}{size=\tiny}

\title{HPCSE II Tutorial: Introduction to CUDA}
\date[]{May 2, 2022}
\author{Vyacheslav Samokhvalov}
\institute{CSElab \\ D-MAVT}

\usetheme{eth}

\colorlet{titlefgcolor}{ETHBlue}
\colorlet{accentcolor}{ETHRed}

\begin{document}

%\def\titlefigure{elements/title-page-image}		% Default image
%\def\titlefigure{elements/title-page-image-43}	% Use this for 4:3 presentations

\titleframe

%%\colorlet{titlefgcolor}{ETHPurple}
%%\def\titlefigure{elements/title-page-image-alt}
%%\title{Different background}
%%\titleframe

%%\colorlet{titlebgcolor}{ETHGreen}
%%\def\titlefigure{}
%%\setlength{\titleboxwidth}{0.75\textwidth}			% Change box width
%%\title{Or even a plain color, especially if your title is very long and leaves no space for what's behind the colored box}
%%\titleframe

%\tocframe

\begin{frame}[fragile]{Vector Addition}
  \begin{columns}
    \begin{column}{0.7\textwidth}
      Recall the vector addition kernel:
      \begin{minted}{cuda}
  __global__ void add(int n, float *a, float *b, float *c)
  {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < n)
      c[i] = a[i] + b[i];
  }
      \end{minted}
      \begin{figure}
        \includegraphics[width=0.9\textwidth]{figs/vector-add-threads.png}
      \end{figure}
       which can be called from host with:
      \begin{minted}{cuda}
  add<<<n/ThreadsPerBlock+1, ThreadsPerBlock>>>(n, a, b, c);
      \end{minted}
    \end{column}
    \begin{column}{0.3\textwidth}
      \begin{figure}
        \includegraphics[width=0.9\textwidth]{figs/vector-add-blocks.png}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}[fragile]{CUDA Programming Model}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item The CUDA parallel programming model aims to provide automatic scalability
          across a range of NVIDIA GPU families, from mainstream GeForce to professional
          Tesla and Volta families.
      \item The blocks in the \verb-add- kernel would automatically fill up the SM's
        available on the device.
      \item To write really good kernels, the programmer still needs to take
        into account hardware considerations and employ some "best practices".
      \item These may vary slightly accross GPU architectures, in this
        presentation we focus on compute capability 6.0.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=0.9\textwidth]{figs/automatic-scalability.png}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Pascal GPU Architecture}
  \begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/gp100_block_diagram-1-624x368.png}
      \end{figure}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{itemize}
        \item The GPU is composed of Streaming Multiprocessors (SM's) each of
          which creates, manages, schedules and executes instructions for many
          threads in parallel.
        \item It has a large area dedicated to ALU's and relatively small area
          dedicated to control units.
        \item DRAM bandwidth an order of magnitude higher than that of a CPU.
      \end{itemize}
    \end{column}
  \end{columns}
  \vfill
  \begin{itemize}
    \item[$\blacktriangleright$] Optimized for data-parallel computation
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Pascal GPU Architecture}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/gp100_SM_diagram-624x452.png}
      \end{figure}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{itemize}
        \item The SM schedules and executes warps (teams of 32 threads).  A warp
          scheduler has to issue the same instruction for all threads of a warp.
          \begin{itemize}
            \item e.g. Pascal has two warp schedulers, meaning it can schedule
              different instructions to two sets of 32 threads independently.
            \item The amount of time it takes to actually execute these
              instructions depends on the instruction throughput of the SM for
              the particular instruction issued\footnotemark.
          \end{itemize}
        \item The SM has fast on-chip memory:
          \begin{itemize}
            \item Registers
            \item Shared memory
            \item L1 Cache
          \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
  \footnotetext{\url{https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html\#maximize-instruction-throughput}}
\end{frame}

\begin{frame}[fragile]{How GPU Computing Works\footnotemark}
      \begin{figure}
        \includegraphics[width=0.6\textwidth]{figs/warp-scheduling.png}
      \end{figure}
      \begin{itemize}
        \item An \textit{eligible warp} is a warp which is ready to execute its
          next instruction because all of its input operands are available.
        \item While an eligible warp is doing work, the device is fetching input
          operands (instructions, memory) necessary for the currently
          \textit{stalled warps} to become eligible.
        \item The eligible warp is masking the latency and transaction time of
          input operands for stalled warps.
          \begin{itemize}
            \item If some input operand resides in off-chip memory, the latency
              is high, typically hundreds of clock cycles.
          \end{itemize}
      \end{itemize}
  \footnotetext{\url{https://www.nvidia.com/en-us/on-demand/session/gtcspring21-s31151/}}
\end{frame}

\begin{frame}[fragile]{GPU Parallelism}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/gpu-parallelism.png}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item Warps of multiple blocks can be reside on the same SM.
        \item Whether or not one or multiple blocks are mapped on a single SM
          depends on:
          \begin{itemize}
            \item Amount of registers and shared memory used the the
              kernel/available on the multiprocessor.
            \item Maximum number of resident blocks and warps per multiprocessor
          \end{itemize}
        \item These are a function of compute capability of the device.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{How GPU Computing Works}
  To summarize empirically, as a programmer you have to take care of the
  following:
  \\
  \begin{itemize}
    \item Get the data to your warps as fast as possible, and use it as
      efficiently as possible:
      \begin{itemize}
        \item Data reuse wherever possible to reduce bandwidth
        \item Data as close to compute as possible, take advantage of shared
          memory and registers
      \end{itemize}
    \item Expose as much parallelism of your problem as possible:
      \begin{itemize}
        \item Reduce dependency between the warps
        \item Instruction level parallelism
        \item Avoid branch divergence
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{GPU Memory Types}
  Global:
  \begin{itemize}
    \item Resides on device RAM, can be cached in L1 or L2.
    \item Allocated/deallocated on host using \verb-cudaMalloc- and
      \verb-cudaFree-.
    \item Has global scope, accessible by all threads in grid.
  \end{itemize}
  Shared:
  \begin{itemize}
    \item Resides on on-chip (SM), has much higher bandwidth and much lower
      latency than DRAM.
    \item Statically or dynamically allocated for each kernel.
    \item Has scope and lifetime of a thread block.
  \end{itemize}
  Local:
  \begin{itemize}
    \item Can reside in registers or DRAM depending on circumstances.
    \item Local variables or arrays statically allocated within a kernel.
    \item Has the scope of a thread.
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Accessing Global Memory}
  \textbf{High Priority: } Global memory is slow, ensure accesses are coalesced 
  whenever possible.
  \vfill
  What is coalesced access?
  \begin{itemize}
    \item Memory transactions from global memory happen in aligned 32 byte
      segments.
    \item A single transaction can service multiple threads of a warp.
  \end{itemize}
  \vfill
  \textit{For devices of compute capability 6.0 or higher, the requirements can
  be summarized quite easily: the concurrent accesses of the threads of a warp
  will coalesce into a number of transactions equal to the number of 32-byte
  transactions necessary to service all of the threads of the warp\footnotemark.}
  \footnotetext{\url{https://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html\#coalesced-access-to-global-memory}}
\end{frame}

\begin{frame}[fragile]{Accessing Global Memory}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      Threads of a warp access adjacent 4-byte words, four coalesced 32-byte
      transactions will service this memory access pattern:
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{figs/coalesced-access.png}
      \end{figure}
      In case of misaligned access, five 32-byte transactions will be necessary:
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{figs/misaligned-sequential-addresses.png}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=0.7\textwidth]{figs/performance-of-stridecopy-kernel.png}
      \end{figure}
      \begin{figure}
        \includegraphics[width=0.7\textwidth]{figs/performance-of-offsetcopy-kernel.png}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Accessing Global Memory}
  L1/L2 caching depends on the type of transaction:
  \vfill
  \begin{itemize}
    \item \textit{Data that is not read-only for the entire lifetime of the
      kernel cannot be cached in the unified L1/texture cache for devices of
      compute capability 5.0.} (also true for CC 6.0)
    \item \textit{A cache line is 128 bytes and maps to a 128 byte aligned
      segment in device memory. Memory accesses that are cached in both L1 and
      L2 are serviced with 128-byte memory transactions, whereas memory accesses
      that are cached in L2 only are serviced with 32-byte memory transactions.}
    \item \textit{Each memory request is then broken down into cache line
      requests that are issued independently. A cache line request is serviced
      at the throughput of L1 or L2 cache in case of a cache hit, or at the
      throughput of device memory, otherwise.}
  \end{itemize}
  \vfill
  For more architecture specific details, consult
  \href{https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html\#compute-capabilities}{Compute
  Capabilities} section of the CUDA C Programming Guide
\end{frame}

\begin{frame}[fragile]{Accessing Global Memory}
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{figs/examples-of-global-memory-accesses.png}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Shared Memory}
  \begin{itemize}
    \item On-chip memory with much higher bandwidth and lower-latency than
      uncached global memory.
    \item Explicity allocated/accessed by the programmer.
    \item Has the scope of the block, i.e. data in shared memory visible to all
      threads within a block:
      \begin{itemize}
        \item Race conditions are possible in case of data sharing between
          threads
        \item \verb-__syncthreads()- a barrier primitive to synchronize all
          threads of a block
      \end{itemize}
    \item Use cases:
      \begin{itemize}
        \item user-managed data cache
        \item work space for cooperative parallel algorithms, e.g reduction
        \item mitigation of uncoalesced global memory accessing
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Static Shared Memory}
  Shared memory size must be known at compile time:
  \vfill
  \begin{minted}{cuda}
  __global__ void staticReverse(double *d)
  {
    __shared__ double s[64];
    int t = threadIdx.x;
    int tr = 64-t-1;
    s[t] = d[t];
    __syncthreads();
    d[t] = s[tr];
  }
  \end{minted}
  \vfill
	invoked as from host as follows:
  \vfill
  \begin{minted}{cuda}
  staticReverse<<<1, 64>>>(d);
  \end{minted}
  \vfill
  Similarly for 2D arrays:
  \vfill
  \begin{minted}{cuda}
  __shared__ int s[64][64];
  \end{minted}

\end{frame}
\begin{frame}[fragile]{Dynamic Shared Memory}
  Allocated as a contiguous block, layout of the arrays to
  be explicitly managed by the programmer through offsets, multi-dimensionality
  via linear indexing:
  \begin{minted}{cuda}
  __global__ void dynamicReverse(int n, int* i, double* d)
  {
    extern __shared__ char s[];
    int*    si = (int*)&s[0];
    double* sd = (double*)&si[n];
    int t = threadIdx.x;
    int tr = n-t-1;
    si[t] = i[t];
    sd[t] = d[t];
    __syncthreads();
    i[t] = si[tr];
    d[t] = sd[tr];
  }
  \end{minted}
  \vfill
	dynamically allocated at runtime using the third kernel execution parameter:
  \vfill
  \begin{minted}{cuda}
  dynamicReverse<<<1, n, n*sizeof(int) + n*sizeof(double)>>>(n, i, d);
  \end{minted}
  \vfill
\end{frame}

\begin{frame}[fragile]{Shared Memory}{Example: Coalesced Transpose\footnotemark}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/naiveTranspose-1024x409.png}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
__global__ void transposeNaive(float *odata, const float *idata)
{
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j+= BLOCK_ROWS)
    odata[x*width + (y+j)] = idata[(y+j)*width + x];
}
      \end{minted}
      \begin{itemize}
        \item Global memory reads are coalesced
        \item Each thread copies \verb-TILE_DIM / BLOCK_ROWS- elements
        \item Global memory writes with stride 
          \verb-gridDim.x * TILE_DIM-
      \end{itemize}
    \end{column}
  \end{columns}
  \footnotetext{\url{https://developer.nvidia.com/blog/efficient-matrix-transpose-cuda-cc/}}
\end{frame}

\begin{frame}[fragile]{Shared Memory}{Example: Coalesced Transpose}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/sharedTranspose-1024x409.jpg}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
__global__ void transposeCoalesced(float *odata, const float *idata)
{
  __shared__ float tile[TILE_DIM][TILE_DIM];
    
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     tile[threadIdx.y+j][threadIdx.x] = idata[(y+j)*width + x];

  __syncthreads();

  // transpose block offset
  x = blockIdx.y * TILE_DIM + threadIdx.x;
  y = blockIdx.x * TILE_DIM + threadIdx.y;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     odata[(y+j)*width + x] = tile[threadIdx.x][threadIdx.y + j];
}
      \end{minted}
      \begin{itemize}
        \item Coalesced global memory reads into shared memory
        \item Columns are read from fast shared memory and written back as rows 
          into global memory
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Automatic Variables}
  An automatic variable declared in device code without any of the
  \verb-__device__-, \verb-__shared__- and \verb-__constant__- specifiers
  generally resides in a register.  These variables are private to each
  individual thread:
  \\
  \begin{minted}{cuda}
  __global__ void kernel(int* a)
  {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    int arr[3] = {tid, 2*tid, 3*tid};
  }
  \end{minted}
  
  Note: "automatic" and "local" are occasionally used interchangeably, both in
  reference to automatic variables.
\end{frame}

\begin{frame}[fragile]{Local Memory}
  The compiler may choose to place automatic variables in \textit{local
  memory}, which has adverse effects on performance.  Likely candidates are:
  \vfill
  \begin{itemize}
    \item Arrays for which it cannot determine that they are indexed with
      constant quantities
    \item Large structures or arrays that would consume too much register space
    \item Any variable if the kernel uses more registers than available (this is
      also known as register spilling)
  \end{itemize}
  \vfill
  Local memory space resides in DRAM, so local memory accesses have the high
  latency and low bandwidth of global memory with identical caching. \textbf{
  Accessing automatic variables may be latency free, or as expensive as global 
  memory access!}
  \vfill
  Inspecting PTX assembly code will tell if a variable has been placed in local
  memory or a register. \\
  e.g. \url{https://cuda.godbolt.org/z/E7rPE6h1o}
\end{frame}
\end{document}
