/* Copyright (c) 1993-2015, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Modified from 'transpose.cu' by Vyacheslav Samokhvalov
 */

#include <stdio.h>
#include <assert.h>
#include <cuda_profiler_api.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

const int NUM_REPS = 100;

// Check errors and print GB/s
void postprocess(const float *ref, const float *res, int n, float ms)
{
  bool passed = true;
  for (int i = 0; i < n; i++)
    if (abs(res[i]-ref[i]) > 1e-5) {
      printf(": %d %f %f\n", i, res[i], ref[i]);
      printf("%25s\n", "*** FAILED ***");
      passed = false;
      break;
    }
  if (passed)
    printf("%20.4f\n", ms/NUM_REPS );
}
__global__ void saxpy1(int n, float a, float *x, float *y)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n) 
    y[i] = a * x[i] + y[i];
}
__global__ void saxpy2(int n, float a, float *x, float *y)
{
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; 
       i < n; 
       i += blockDim.x * gridDim.x) 
  {
    y[i] = a * x[i] + y[i];
  }
}

int main()
{
  const int n = 4194304; // 2^22
  const int mem_size = n * sizeof(float);

  float *h_x    = (float*)malloc(mem_size);
  float *h_y    = (float*)malloc(mem_size);
  float *h_res  = (float*)malloc(mem_size);
  float *gold   = (float*)malloc(mem_size);
  
  float *d_x, *d_y;
  checkCuda( cudaMalloc(&d_x, mem_size) );
  checkCuda( cudaMalloc(&d_y, mem_size) );

  // host
  float a = 3.14;
  for (int i = 0; i < n; i++)
  {
    h_x[i] = (float)(i%17);
    h_y[i] = (float)((n-1-i)%31);
    gold[i] = a * h_x[i] + h_y[i];
  }


  // device
  checkCuda( cudaMemcpy(d_x, h_x, mem_size, cudaMemcpyHostToDevice) );
  checkCuda( cudaMemcpy(d_y, h_y, mem_size, cudaMemcpyHostToDevice) );
  
  // events for timing
  cudaEvent_t startEvent, stopEvent;
  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );
  float ms;

  // ------------
  // time kernels
  // ------------
  printf("%15s%25s\n", "Routine", "Runtine [ms]");
  
  // ----
  // saxpy1 
  // ----
  printf("%15s", "saxpy1");
  // warm up
  checkCuda(cudaProfilerStart());
  saxpy1<<<n/512+1, 512>>>(n, a, d_x, d_y);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    saxpy1<<<n/512+1, 512>>>(n, a, d_x, d_y);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  // cool down
  checkCuda( cudaMemcpy(d_y, h_y, mem_size, cudaMemcpyHostToDevice) );
  saxpy1<<<n/512+1, 512>>>(n, a, d_x, d_y);
  checkCuda( cudaMemcpy(h_res, d_y, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(gold, h_res, n, ms);

  // ----
  // saxpy2
  // ----
  printf("%15s", "saxpy2");
  // warm up
  checkCuda(cudaProfilerStart());
  saxpy2<<<8*56, 256>>>(n, a, d_x, d_y);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    saxpy2<<<8*56, 256>>>(n, a, d_x, d_y);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  // cool down
  checkCuda( cudaMemcpy(d_y, h_y, mem_size, cudaMemcpyHostToDevice) );
  saxpy2<<<8*56, 256>>>(n, a, d_x, d_y);
  checkCuda( cudaMemcpy(h_res, d_y, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(gold, h_res, n, ms);
  
  // cleanup
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
  checkCuda( cudaFree(d_x) );
  checkCuda( cudaFree(d_y) );
  free(h_x);
  free(h_y);
  free(h_res);
  free(gold);
}
