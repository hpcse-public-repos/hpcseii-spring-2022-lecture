/* Copyright (c) 1993-2015, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Modified from 'transpose.cu' by Vyacheslav Samokhvalov
 */

#include <stdio.h>
#include <assert.h>
#include <cuda_profiler_api.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

const int NUM_REPS = 100;

// Check errors and print GB/s
void postprocess(const float *ref, const float *res, int n, float ms)
{
  bool passed = true;
  for (int i = 0; i < n; i++)
    if (res[i] != ref[i]) {
      printf(": %d %f %f\n", i, res[i], ref[i]);
      printf("%25s\n", "*** FAILED ***");
      passed = false;
      break;
    }
  if (passed)
    printf("%20.4f\n", ms / NUM_REPS);
}

__global__ void outer_product1(int nx, int ny, float *prod, const float *x, const float *y)
{
  const int ix = blockIdx.x * blockDim.x + threadIdx.x;
  const int iy = blockIdx.y * blockDim.y + threadIdx.y;

  prod[iy*nx + ix] = x[ix]*y[iy];
}

__global__ void outer_product2(int nx, int ny, float *prod, const float *x, const float *y)
{
  const int ix = blockIdx.x * blockDim.x + threadIdx.x;
  const int iy = blockIdx.y * blockDim.y + threadIdx.y;
  const int gsx = gridDim.x * blockDim.x;
  const int gsy = gridDim.y * blockDim.y;
  float a[4] = {x[ix], x[ix+gsx], x[ix+2*gsx], x[ix+3*gsx]};
  float b[4] = {y[iy], y[iy+gsy], y[iy+2*gsy], y[iy+3*gsy]};
  #pragma unroll
  for (int j = 0; j < 4; j++)
  #pragma unroll
  for (int i = 0; i < 4; i++)
    prod[(iy+j*gsy)*nx + ix+i*gsx] = a[i]*b[j];
}

__global__ void outer_product3(int nx, int ny, float *prod, const float *x, const float *y)
{
  const int ix = 4*(blockIdx.x * blockDim.x + threadIdx.x);
  const int iy = 4*(blockIdx.y * blockDim.y + threadIdx.y);
  float a[4] = {x[ix], x[ix+1], x[ix+2], x[ix+3]};
  float b[4] = {y[iy], y[iy+1], y[iy+2], y[iy+3]};
  #pragma unroll
  for (int j = 0; j < 4; j++)
  #pragma unroll
  for (int i = 0; i < 4; i++)
    prod[(iy+j)*nx + ix+i] = a[i]*b[j];
}

int main()
{
  const int nx = 2048;
  const int ny = 2048;

  float *h_x    = (float*)malloc(nx*sizeof(float));
  float *h_y    = (float*)malloc(ny*sizeof(float));
  float *h_prod = (float*)malloc(nx*ny*sizeof(float));
  float *gold   = (float*)malloc(nx*ny*sizeof(float));
  
  float *d_x, *d_y, *d_prod;
  checkCuda( cudaMalloc(&d_x, nx*sizeof(float)) );
  checkCuda( cudaMalloc(&d_y, ny*sizeof(float)) );
  checkCuda( cudaMalloc(&d_prod, nx*ny*sizeof(float)) );

  // host
  for (int i = 0; i < nx; i++)
    h_x[i] = (float)i;
  for (int i = 0; i < ny; i++)
    h_y[i] = (float)(ny-1-i);
  // gold standard, column major h_x h_y^T
  for (int i = 0; i < ny; i++)
  for (int j = 0; j < nx; j++)
    gold[i*ny+j] = h_x[j]*h_y[i];


  // device
  checkCuda( cudaMemcpy(d_x, h_x, nx*sizeof(float), cudaMemcpyHostToDevice) );
  checkCuda( cudaMemcpy(d_y, h_y, ny*sizeof(float), cudaMemcpyHostToDevice) );
  
  // events for timing
  cudaEvent_t startEvent, stopEvent;
  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );
  float ms;

  // ------------
  // time kernels
  // ------------
  printf("%15s%25s\n", "Routine", "Runtine [ms]");
  
  // ----
  // memcpy1 
  // ----
  printf("%15s", "outer_product1");
  checkCuda( cudaMemset(d_prod, 0, nx*ny*sizeof(float)) );
  // warm up
  checkCuda(cudaProfilerStart());
  outer_product1<<<{nx/32, ny/32}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    outer_product1<<<{nx/32, ny/32}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_prod, d_prod, nx*ny*sizeof(float), cudaMemcpyDeviceToHost) );
  postprocess(gold, h_prod, nx*ny, ms);
  
  // ----
  // memcpy2 
  // ----
  printf("%15s", "outer_product2");
  checkCuda( cudaMemset(d_prod, 0, nx*ny*sizeof(float)) );
  // warm up
  checkCuda(cudaProfilerStart());
  outer_product2<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    outer_product2<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_prod, d_prod, nx*ny*sizeof(float), cudaMemcpyDeviceToHost) );
  postprocess(gold, h_prod, nx*ny, ms);
  
  // ----
  // memcpy2 
  // ----
  printf("%15s", "outer_product3");
  checkCuda( cudaMemset(d_prod, 0, nx*ny*sizeof(float)) );
  // warm up
  checkCuda(cudaProfilerStart());
  outer_product3<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    outer_product3<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_prod, d_prod, nx*ny*sizeof(float), cudaMemcpyDeviceToHost) );
  postprocess(gold, h_prod, nx*ny, ms);


  // cleanup
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
  checkCuda( cudaFree(d_x) );
  checkCuda( cudaFree(d_y) );
  checkCuda( cudaFree(d_prod) );
  free(h_x);
  free(h_y);
  free(h_prod);
  free(gold);
}
