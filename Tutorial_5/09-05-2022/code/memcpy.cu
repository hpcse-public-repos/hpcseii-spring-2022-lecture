/* Copyright (c) 1993-2015, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Modified from 'transpose.cu' by Vyacheslav Samokhvalov
 */

#include <stdio.h>
#include <assert.h>
#include <cuda_profiler_api.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

const int NUM_REPS = 100;

// Check errors and print GB/s
void postprocess(const float *ref, const float *res, int n, float ms)
{
  bool passed = true;
  for (int i = 0; i < n; i++)
    if (res[i] != ref[i]) {
      printf("%d %f %f\n", i, res[i], ref[i]);
      printf("%25s\n", "*** FAILED ***");
      passed = false;
      break;
    }
  if (passed)
    printf("%20.2f\n", 2 * n * sizeof(float) * 1e-6 * NUM_REPS / ms );
}

__global__ void myMemcpy1(int n, float *odata, const float *idata)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  odata[i] = idata[i];
}

#define WORKLOAD 4
__global__ void myMemcpy2(int n, float *odata, const float *idata)
{
  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  const int gs = gridDim.x * blockDim.x;
  float a[WORKLOAD];
  #pragma unroll
  for (int j=0; j < WORKLOAD; j++)
    a[j] = idata[i+j*gs];

  #pragma unroll
  for (int j=0; j < WORKLOAD; j++)
    odata[i+j*gs] = a[j];
}

int main()
{
  const int n = 4194304; // 2^22
  const int mem_size = n*sizeof(float);

  float *h_idata = (float*)malloc(mem_size);
  float *h_odata = (float*)malloc(mem_size);
  
  float *d_idata, *d_odata;
  checkCuda( cudaMalloc(&d_idata, mem_size) );
  checkCuda( cudaMalloc(&d_odata, mem_size) );

  // host
  for (int i = 0; i < n; i++)
    h_idata[i] = (float)i;

  // device
  checkCuda( cudaMemcpy(d_idata, h_idata, mem_size, cudaMemcpyHostToDevice) );
  
  // events for timing
  cudaEvent_t startEvent, stopEvent;
  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );
  float ms;

  // ------------
  // time kernels
  // ------------
  printf("%15s%15s%25s\n", "Routine", "dimBlock", "Bandwidth (GB/s)");
  
  // ----
  // memcpy1 
  int NUM_THREADS = 128;
  // ----
  printf("%15s%15d", "myMemcpy1", NUM_THREADS);
  checkCuda( cudaMemset(d_odata, 0, mem_size) );
  // warm up
  checkCuda(cudaProfilerStart());
  myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
     myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_odata, d_odata, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(h_idata, h_odata, n, ms);
  
  // ----
  // memcpy1 
  NUM_THREADS = 64;
  // ----
  printf("%15s%15d", "myMemcpy1", NUM_THREADS);
  checkCuda( cudaMemset(d_odata, 0, mem_size) );
  // warm up
  checkCuda(cudaProfilerStart());
  myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
     myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_odata, d_odata, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(h_idata, h_odata, n, ms);
  
  // ----
  // memcpy1 
  NUM_THREADS = 32;
  // ----
  printf("%15s%15d", "myMemcpy1", NUM_THREADS);
  checkCuda( cudaMemset(d_odata, 0, mem_size) );
  // warm up
  checkCuda(cudaProfilerStart());
  myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
     myMemcpy1<<<n/NUM_THREADS, NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_odata, d_odata, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(h_idata, h_odata, n, ms);

  // ----
  // memcpy2 
  NUM_THREADS = 32;
  // ----
  printf("%15s%15d", "myMemcpy2", NUM_THREADS);
  checkCuda( cudaMemset(d_odata, 0, mem_size) );
  // warm up
  checkCuda(cudaProfilerStart());
  myMemcpy2<<<n/(NUM_THREADS*WORKLOAD), NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda(cudaProfilerStop());
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
     myMemcpy2<<<n/(NUM_THREADS*WORKLOAD), NUM_THREADS>>>(n, d_odata, d_idata);
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_odata, d_odata, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(h_idata, h_odata, n, ms);

  // ----
  // cudaMemcpy 
  // ----
  printf("%15s%15s", "cudaMemcpy", "N/A");
  checkCuda( cudaMemset(d_odata, 0, mem_size) );
  // warm up
  checkCuda( cudaMemcpyAsync(d_odata, d_idata, mem_size, cudaMemcpyDeviceToDevice, 0) );
  checkCuda( cudaEventRecord(startEvent, 0) );
  for (int i = 0; i < NUM_REPS; i++)
    checkCuda( cudaMemcpyAsync(d_odata, d_idata, mem_size, cudaMemcpyDeviceToDevice, 0) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  checkCuda( cudaEventElapsedTime(&ms, startEvent, stopEvent) );
  checkCuda( cudaMemcpy(h_odata, d_odata, mem_size, cudaMemcpyDeviceToHost) );
  postprocess(h_idata, h_odata, n, ms);

  // cleanup
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
  checkCuda( cudaFree(d_odata) );
  checkCuda( cudaFree(d_idata) );
  free(h_idata);
  free(h_odata);
}
