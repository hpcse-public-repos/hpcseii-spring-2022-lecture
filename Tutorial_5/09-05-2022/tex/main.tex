%Please use LuaLaTeX or XeLaTeX
\documentclass[11pt,aspectratio=169]{beamer}
\usepackage{minted}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{tcolorbox}
\usepackage[export]{adjustbox}
\setbeamerfont{footnote}{size=\tiny}

\title{HPCSE II Tutorial: Occupancy, Bank Conflicts}
\date[]{May 9, 2022}
\author{Vyacheslav Samokhvalov}
\institute{CSElab \\ D-MAVT}

\usetheme{eth}

\colorlet{titlefgcolor}{ETHBlue}
\colorlet{accentcolor}{ETHRed}

\begin{document}

%\def\titlefigure{elements/title-page-image}		% Default image
%\def\titlefigure{elements/title-page-image-43}	% Use this for 4:3 presentations

\titleframe

%%\colorlet{titlefgcolor}{ETHPurple}
%%\def\titlefigure{elements/title-page-image-alt}
%%\title{Different background}
%%\titleframe

%%\colorlet{titlebgcolor}{ETHGreen}
%%\def\titlefigure{}
%%\setlength{\titleboxwidth}{0.75\textwidth}			% Change box width
%%\title{Or even a plain color, especially if your title is very long and leaves no space for what's behind the colored box}
%%\titleframe

%\tocframe

\begin{frame}[fragile]{Recap}
      \begin{figure}
        \includegraphics[width=0.6\textwidth]{figs/warp-scheduling.png}
      \end{figure}
      \begin{itemize}
        \item An \textit{eligible warp} is a warp which is ready to execute its
          next instruction because all of its input operands are available.
        \item While an eligible warp is doing work, the device is fetching input
          operands (instructions, memory) necessary for the currently
          \textit{stalled warps} to become eligible.
        \item The eligible warp is masking the latency and transaction time of
          input operands for stalled warps.
          \begin{itemize}
            \item If some input operand resides in off-chip memory, the latency
              is high, typically hundreds of clock cycles.
          \end{itemize}
      \end{itemize}
\end{frame}

\begin{frame}[fragile]{Utilization}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/littles-law.png}
      \end{figure}
      \textbf{Little's Law:} $P=LT$
      \begin{itemize}
        \item $P$ - parallelism [instructions/SM]
        \item $L$ - latency     [cycles]
        \item $T$ - throughput  [instructions/SM/cycle]
      \end{itemize}
    \end{column}
    \begin{column}{0.55\textwidth}
      Two options to hide latencies:
      \begin{enumerate}
        \item \textit{Data-Level parallelism} = to have many different warps
          working on distinct memory locations
        \item \textit{Instruction-Level parallelism} = same or different warps
          executing multiple instructions that have no data dependency
      \end{enumerate}
      Grossly speaking, for full utilization you can either:
      \begin{enumerate}
        \item Have $P$ threads each schedule one instruction every $L$ cycles,
          but can your memory keep up?
        \item Have $N$ threads each schedule $P/N$ instructions every $L$
          cycles, but is there enough inherent ILP in your algorithm?
      \end{enumerate}
    \end{column}
  \end{columns}
  \vfill
  Note: $N$ must be at least $32\times$[number of warp schedulers] your SM has!
\end{frame}

\begin{frame}[fragile]{Utilization}
  Intuitively, two ways to try and maximize utilization:
  \\
  \begin{enumerate}
    \item Increase the number of warps that can run on an SM:
      \begin{itemize}
        \item Schedule more warps per SM, more warps = potentially more active warps
        \item Limited by hardware resources
      \end{itemize}
    \item Increase the amount of "work" each active warp can do:
      \begin{itemize}
        \item Reuse data for multiple computations (increase amount of work done
          per memory load to registers)
        \item Ensure data is as close to chip as possible to minimize latencies
          (reduce the amount of time a warp is inactive)
        \item Inherently limited by the algorithm (arithmetic intensity)
      \end{itemize}
  \end{enumerate}
  (In most cases) impossible to maximize utilization by considering only one of
  the two points above, need to strike balance.
\end{frame}

\begin{frame}[fragile]{Occupancy}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      Occupancy is the ratio between the active warps per SM in a given kernel
      launch versus the maximum number of warps that could run on the SM:
      \vfill
      \begin{tcolorbox}
        \begin{equation*}
          \text{Occupancy} = \frac{\text{Active warps per SM}}{\text{Max. warps per SM}}
        \end{equation*}
      \end{tcolorbox}
      \vfill
      The variables which affect occupancy of a kernel depend on the compute
      capability of the GPU
    \end{column}
    \begin{column}{0.6\textwidth}
\begin{table}[]
\resizebox{\textwidth}{!}{
\begin{tabular}{|p{4cm}|l|l|l|l}
\cline{1-4}
Compute Capability                             & 3.5         & 5.2   & 6.0   &  \\ \cline{1-4}
Threads / Warp                                 & 32          & 32    & 32    &  \\ \cline{1-4}
Max Warps / Multiprocessor                     & 64          & 64    & 64    &  \\ \cline{1-4}
Max Threads / Multiprocessor                   & 2048        & 2048  & 2048  &  \\ \cline{1-4}
Max Thread Blocks / Multiprocessor             & 16          & 32    & 32    &  \\ \cline{1-4}
Max 32-bit Registers / SM                      & 65536       & 65536 & 65536 &  \\ \cline{1-4}
Max Registers / Block                          & 65536       & 32768 & 65536 &  \\ \cline{1-4}
Max Registers / Thread                         & 255         & 255   & 255   &  \\ \cline{1-4}
Max Thread Block Size                          & 1024        & 1024  & 1024  &  \\ \cline{1-4}
CUDA Cores / SM                                & 192         & 128   & 64    &  \\ \cline{1-4}
Shared Memory Size / SM Configurations (bytes) & 16K/32K/48K & 96K   & 64K   &  \\ \cline{1-4}
\end{tabular}
} % resizebox
\end{table}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Occupancy}
  The occupancy achieved by each kernel depends on the resources
  allocated per thread-block vs. resources available on SM.  Your limitations
  are:
  \\
  \begin{itemize}
    \item Register usage per thread
    \item Shared memory usage per block
    \item Launch configuration
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Occupancy}
  \textbf{Example: limited by register usage}
  Compute capability 6.0:
  \begin{itemize}
    \item Maximum threads per SM is 2048
    \item 65536 32-bit registers
  \end{itemize}
  \vfill
  \textbf{Case A:} Kernel requires 10 registers per thread
  $\quad\rightarrow\quad$ can launch 6553 threads, SM supports 2048 
  \begin{tcolorbox}[colback=green!25!white]
    \begin{center}
      Occupancy = 100\%
    \end{center}
  \end{tcolorbox}
  \vfill
  \textbf{Case B:} Kernel requires 100 registers per thread
  $\quad\rightarrow\quad$ can launch 655 threads, SM supports 2048 
  \begin{tcolorbox}[colback=yellow!25!white]
    \begin{center}
      Occupancy = 32\%
    \end{center}
  \end{tcolorbox}
  \vfill
  You can limit the number of registers allocated to a kernel using a
  \verb-maxrregcount- option in \verb-nvcc-.  Sometimes a light register spill
  but more active warps may benefit performance.
\end{frame}

\begin{frame}[fragile]{Occupancy}
  \textbf{Example: limited by shared memory}
  Compute capability 6.0:
  \begin{itemize}
    \item Maximum threads per SM is 2048
    \item Maximum shared memory per SM is 64KB
  \end{itemize}
  \vfill
  \textbf{Case A:} Block with 512 threads requires 8KB of \verb-shmem-
  $\quad\rightarrow\quad$ can launch 4096 threads, SM supports 2048 
  \begin{tcolorbox}[colback=green!25!white]
    \begin{center}
      Occupancy = 100\%
    \end{center}
  \end{tcolorbox}
  \vfill
  \textbf{Case B:} Block with 128 threads requires 16KB of \verb-shmem-
  $\quad\rightarrow\quad$ can launch 512 threads, SM supports 2048 
  \begin{tcolorbox}[colback=yellow!25!white]
    \begin{center}
      Occupancy = 25\%
    \end{center}
  \end{tcolorbox}
  \vfill
\end{frame}

\begin{frame}[fragile]{Occupancy}
  \textbf{Example: limited by thread block size}
  Compute capability 6.0:
  \begin{itemize}
    \item Maximum threads per SM is 2048
    \item Maximum thread blocks per SM is 32
  \end{itemize}
  \vfill
  \textbf{Case A:} Kernel with 32 threads per block
  $\quad\rightarrow\quad$ can launch 1024 threads, SM supports 2048 
  \begin{tcolorbox}[colback=yellow!25!white]
    \begin{center}
      Occupancy = 50\%
    \end{center}
  \end{tcolorbox}
\end{frame}

\begin{frame}[fragile]{Occupancy}
  Do I need 100\% occupancy? \textbf{No.} 
  Occupancy is just a metric to be considered in the context of many
  others. High occupancy does not guarantee high utilization (in this case of
  memory bandwidth).
  \vfill
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
    __global__ void myMemcpy1(int n, float *odata, const float *idata)
    {
      int i = blockIdx.x * blockDim.x + threadIdx.x;
      odata[i] = idata[i];
    }
    
    #define WORKLOAD 4
    __global__ void myMemcpy2(int n, float *odata, const float *idata)
    {
      const int i = blockIdx.x * blockDim.x + threadIdx.x;
      const int gs = gridDim.x * blockDim.x;
      float a[WORKLOAD];
      #pragma unroll
      for (int j=0; j < WORKLOAD; j++)
        a[j] = idata[i+j*gs];
    
      #pragma unroll
      for (int j=0; j < WORKLOAD; j++)
        odata[i+j*gs] = a[j];
    }
      \end{minted}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{Verbatim}
     Routine   dimBlock   Bandwidth (GB/s)
   myMemcpy1        128        515.44
   myMemcpy1         32        136.10
   myMemcpy2         32        503.10
  cudaMemcpy        N/A        498.94
      \end{Verbatim}
      \begin{itemize}
        \item \verb-myMemcpy1- achieves full theoretical occupancy for
          appropriate choice of thread block size
        \item \verb-myMemcpy2- achieves  50\% occupancy due choice of thread
          block size
        \item \verb-myMemcpy2- measures comparable bandwidth; lower occupancy is
          compensated by instruction-level parallelism within thread
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Exposing Instruction Level Parallelism}{
    Example: outer product $\mathbf{x}\mathbf{y}^\top$}
  Each thread calculates one element of outer product:
  \vfill
  \begin{minted}{cuda}
    __global__ void outer_product1(int nx, int ny, float *prod, float *x, float *y)
    {
      const int ix = blockIdx.x * blockDim.x + threadIdx.x;
      const int iy = blockIdx.y * blockDim.y + threadIdx.y;

      prod[iy*nx + ix] = x[ix]*y[iy];
    }
  \end{minted}
  \vfill
  Launch configuration:
  \vfill
  \begin{Verbatim}
  outer_product1<<<{nx/32, ny/32}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  \end{Verbatim}
  \vfill
  Runtime for \verb-nx = ny = 2048- is 0.0470 [ms].
\end{frame}

\begin{frame}[fragile]{Exposing Instruction Level Parallelism}{
    Example: outer product $\mathbf{x}\mathbf{y}^\top$}
  Each thread calculates four elements of outer product:
  \vfill
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
    __global__ void outer_product2(int nx, int ny, float *prod, float *x, float *y)
    {
      const int ix = blockIdx.x * blockDim.x + threadIdx.x;
      const int iy = blockIdx.y * blockDim.y + threadIdx.y;
      const int gsx = gridDim.x * blockDim.x;
      const int gsy = gridDim.y * blockDim.y;
      float a[4] = {x[ix], x[ix+gsx], x[ix+2*gsx], x[ix+3*gsx]};
      float b[4] = {y[iy], y[iy+gsy], y[iy+2*gsy], y[iy+3*gsy]};
      #pragma unroll
      for (int j = 0; j < 4; j++)
      #pragma unroll
      for (int i = 0; i < 4; i++)
        prod[(iy+j*gsy)*nx + ix+i*gsx] = a[i]*b[j];
    }
      \end{minted}
    \end{column}
    \begin{column}{0.5\textwidth}
      \only<1>{
        \begin{figure}
          \includegraphics[width=0.5\textwidth,right]{figs/tile-structure.png}
      \end{figure}
      }
      \only<2>{
        \begin{itemize}
          \item Each tile retains the global memory access pattern of previous kernel
          \item A single load to \texttt{a} and \texttt{b} is now re-used four times
          \item Arrays \texttt{a} and \texttt{b} stored in registers
        \end{itemize}
      }
    \end{column}
  \end{columns}
  \vfill
  Launch configuration:
  \vfill
  \begin{Verbatim}
  outer_product2<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  \end{Verbatim}
  \vfill
  Runtime for \verb-nx = ny = 2048- is 0.0365 [ms].
\end{frame}

\begin{frame}[fragile]{Exposing Instruction Level Parallelism}{
    Example: outer product $\mathbf{x}\mathbf{y}^\top$}
  Applying ILP blindly may actually make things worse:
  \vfill
  \begin{minted}{cuda}
    __global__ void outer_product3(int nx, int ny, float *prod, float *x, float *y)
    {
      const int ix = 4*(blockIdx.x * blockDim.x + threadIdx.x);
      const int iy = 4*(blockIdx.y * blockDim.y + threadIdx.y);
      float a[4] = {x[ix], x[ix+1], x[ix+2], x[ix+3]};
      float b[4] = {y[iy], y[iy+1], y[iy+2], y[iy+3]};
      #pragma unroll
      for (int j = 0; j < 4; j++)
      #pragma unroll
      for (int i = 0; i < 4; i++)
        prod[(iy+j)*nx + ix+i] = a[i]*b[j];
    }
  \end{minted}
  \vfill
  Launch configuration:
  \vfill
  \begin{Verbatim}
  outer_product2<<<{nx/(32*4), ny/(32*4)}, {32, 32}>>>(nx, ny, d_prod, d_x, d_y);
  \end{Verbatim}
  \vfill
  Runtime for \verb-nx = ny = 2048- is 0.1304 [ms].  Reason: \textbf{sub-optimal global
  memory access pattern!}
\end{frame}

\begin{frame}[fragile]{Shared Memory Access and Bank Conflicts}
  CC 5.x and 6.x:
  \vfill
  \textit{Shared memory has 32 banks that are organized such
  that successive 32-bit words map to successive banks. Each bank has a
  bandwidth of 32 bits per clock cycle. }
  \vfill
  \begin{figure}
    \includegraphics[width=0.5\textwidth]{figs/shared-memory-banks.png}
  \end{figure}
  \vfill
  \textit{A shared memory request for a warp does not generate a bank conflict
  between two threads that access any address within the same 32-bit word (even
  though the two addresses fall in the same bank). In that case, for read
  accesses, the word is broadcast to the requesting threads and for write
  accesses, each address is written by only one of the threads (which thread
  performs the write is undefined). }
\end{frame}

\begin{frame}[fragile]{Shared Memory Access and Bank Conflicts}
  \action<1->{
    \begin{figure}
      \includegraphics[width=0.6\textwidth]{figs/no-bank-conflict.png}
    \end{figure}
  }
  \vfill
  \action<2->{
    No bank conflict.  The read to the same address in bank 0 is broadcast 
    to threads 0 and 1.
  }
\end{frame}

\begin{frame}[fragile]{Shared Memory Access and Bank Conflicts}
  \action<1->{
    \begin{figure}
      \includegraphics[width=0.6\textwidth]{figs/bank-conflict.png}
    \end{figure}
  }
  \vfill
  \action<2->{
    Yes, two way bank conflict.  Two threads attempt to read different
    addresses mapped to the same bank.
  }
\end{frame}

\begin{frame}[fragile]{Shared Memory Access and Bank Conflicts}{Examples of
  Strided Access}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{enumerate}
        \setlength\itemsep{1.5em}
        \item \textbf{Left:} 
          linear addressing with a stride of one 32-bit word (no bank conflict)
        \item \textbf{Middle:} 
          linear addressing with a stride of two 32-bit words (two-way bank conflict)
        \item \textbf{Right:} 
          linear addressing with a stride of three 32-bit words (no bank conflict)
      \end{enumerate}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=0.6\textwidth]{figs/examples-of-strided-shared-memory-accesses.png}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Shared Memory Access and Bank Conflicts}{Examples of
  Irregular Access}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{enumerate}
        \setlength\itemsep{1.5em}
        \item \textbf{Left:} 
          conflict-free access via random permutation
        \item \textbf{Middle:} 
          conflict-free access since threads 3, 4, 6, 7 and 9 access the same
          word within bank 5
        \item \textbf{Right:} 
          conflict-free broadcast access
      \end{enumerate}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=0.6\textwidth]{figs/examples-of-irregular-shared-memory-accesses.png}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Mitigating Bank Conflicts}
  \textbf{1. Modifying access pattern}
  \vfill
  \textit{Assume 4 memory banks and 8 threads in a warp}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{figs/mitigate-access-pattern.png}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Mitigating Bank Conflicts}
  \textbf{2. Padding}
  \vfill
  Recall matrix transpose from last tutorial:
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \only<1>{
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{figs/sharedTranspose-1024x409.jpg}
      \end{figure}
      }
      \only<2>{
        Consider: 
        \begin{itemize}
          \item \texttt{TILE\_DIM=32} 
          \item \texttt{dim3 dimBlock\{TILE\_DIM, BLOCK\_ROWS\}}
        \end{itemize}
        then all elements in a column \texttt{tile[][]} map onto the same shared
        memory bank when writing back to \texttt{odata}.
        \linebreak
        \linebreak
        \textbf{This is a worst case scenario for memory bank conflicts!}
      }
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
__global__ void transposeCoalesced(float *odata, float *idata)
{
  __shared__ float tile[TILE_DIM][TILE_DIM];
    
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     tile[threadIdx.y+j][threadIdx.x] = idata[(y+j)*width + x];

  __syncthreads();

  // transpose block offset
  x = blockIdx.y * TILE_DIM + threadIdx.x;
  y = blockIdx.x * TILE_DIM + threadIdx.y;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     odata[(y+j)*width + x] = tile[threadIdx.x][threadIdx.y + j];
}
      \end{minted}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Mitigating Bank Conflicts}
  \textbf{2. Padding}
  \vfill
  \makebox[\textwidth][s]
  {
    \mintinline[fontsize=\scriptsize]{cuda}{__shared__ float tile[TILE_DIM][TILE_DIM]}
    $\quad\rightarrow\quad$
    \mintinline[fontsize=\scriptsize]{cuda}{__shared__ float tile[TILE_DIM][TILE_DIM+1]}
  }
  \vfill
	\begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \includegraphics[width=0.7\textwidth]{figs/shared-memory-banks-transpose.png}
      \end{figure}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{Verbatim}[fontsize=\tiny]
                Routine     Bandwidth (GB/s)
    coalesced transpose          360.44
conflict-free transpose          462.11
        \end{Verbatim}
    \end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]{Grid-Stride Loops}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
  __global__ void saxpy1(int n, float a, float *x, float *y)
  {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) 
      y[i] = a * x[i] + y[i];
  }
      \end{minted}
    \end{column}
    \begin{column}{0.05\textwidth}
      $\rightarrow$
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{minted}[fontsize=\tiny]{cuda}
  __global__ void saxpy2(int n, float a, float *x, float *y)
  {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x; 
         i < n; 
         i += blockDim.x * gridDim.x) 
      y[i] = a * x[i] + y[i];
  }
      \end{minted}
    \end{column}
  \end{columns}
  \vfill
  \textbf{Scalability:} \verb-saxpy1- is limited by the maximum grid size that can be
  launched on a device executing it, \verb-saxpy2- can operate on vectors of
  arbitrary size.
  \vfill
  \textbf{Flexibility:} \verb-saxpy2- is launch configuration agnostic, a common choice
  for the number of thread block is a multiple of the number of SM's on the
  device executing the kernel.
\end{frame}

\begin{frame}[fragile]{Maximizing Instruction Throughput}
  \begin{itemize}
    \setlength\itemsep{1em}
    \item \texttt{\_\_fdividef(x,y)} provides faster single-precision
      floating-point division than the \texttt{/} operator
    \item Use \texttt{rsqrt(x)} instead of \texttt{1./sqrt(x)}
    \item Integer division and modulo are costly and may compile up to 20
      instructions, if \texttt{n} is a power of two, they can be replaced by
      bit-wise operations
    \item For half-precision operations use intrinsics with \texttt{half2}
      vector datatype
    \item SIMD instrinsics are available for some integer operations
    \item The compiler may insert conversion instructions introducing additional
      cycles, for example, declare \texttt{float a = 1.0f} instead of
      \texttt{float a = 1.0}
  \end{itemize}
  \vfill
  You can consult \href{https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html\#arithmetic-instructions}{arithmetic instructions,} 
  \href{https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html\#intrinsic-functions}{intrinsic functions} 
      and \href{https://docs.nvidia.com/cuda/cuda-math-api/index.html}{CUDA Math API} for further details.
\end{frame}

\begin{frame}[fragile]{PTX}
  PTX provides a stable programming model and instruction set for general
  purpose parallel programming.  It exposes the GPU as a data parallel computing
  device.
  \vfill
  Among the goals of PTX are:
  \begin{itemize}
    \item Achieve performance in compiled applications comparable to native GPU
      performance
    \item Provide a machine-independent ISA for C/C++ and other compilers to
      target
    \item Facilitate hand-coding of libraries, performance kernels, and
      architecture tests
  \end{itemize}
  \vfill
  High level language compilers for languages such as CUDA and C/C++ generate
  PTX instructions, which are optimized for and translated to native
  target-architecture instructions.
  \vfill
  Note that:
  \begin{itemize}
    \item A PTX instruction may not necessarily map to a native instruction,
      some are emulated in software and may be relatively slow.
    \item PTX code produced for some specific compute capability can always be
      compiled to binary code of greater or equal compute capability. Note that
      a binary compiled from an earlier PTX version may not make use of some
      hardware features.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Compilation Workflow}
  \verb-nvcc- is a compiler driver that simplifies the process of compiling C++ or PTX
  code: It provides simple and familiar command line options and executes them
  by invoking the collection of tools that implement the different compilation
  stages.
  \vfill
  \verb-nvcc-'s basic workflow consists in separating device code from host code and
  then: 
  \begin{itemize}
    \item compiling the device code into an assembly form (PTX code) and/or
      binary form (cubin object) if the \texttt{-code} flag specifies target
      achritecture
    \item and modifying the host code by replacing the \verb-<<<...>>>- syntax
      by the necessary CUDA runtime function calls to load and
      launch each compiled kernel from the PTX code and/or cubin object
  \end{itemize}
  \vfill
  Any PTX code loaded by an application at runtime is compiled further to binary
  code by the device driver. This is called just-in-time compilation.
  Just-in-time compilation increases application load time, but allows the
  application to benefit from any new compiler improvements coming with each new
  device driver. It is also the only way for applications to run on devices that
  did not exist at the time the application was compiled.
\end{frame}

\end{document}
